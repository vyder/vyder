use vyder::Library;

pub mod convert;
pub mod env;
pub mod error;
pub mod fmt;
pub mod io;
pub mod map;
pub mod strings;

pub fn get_library() -> Library {
    Library::builder()
        .module("std-convert", convert::get_module())
        .module("std-env", env::get_module())
        .module("std-error", error::get_module())
        .module("std-fmt", fmt::get_module())
        .module("std-io", io::get_module())
        .module("std-map", map::get_module())
        .module("std-strings", strings::get_module())
        .build()
}
