use std::process::ExitCode;

use vyder::{values, Error, ExpectArity, Module, Span, Value, ValueResult};

pub fn insert(
    arguments: &[ValueResult],
    span: Span,
) -> Result<(ValueResult, Option<ExitCode>), Error> {
    let arguments = arguments.to_vec();
    let arguments = arguments.expect_exact::<3>(&span)?;
    let mut map = arguments[0]
        .clone()
        .expect_non_error()?
        .expect::<values::Map>()?;
    let key = arguments[1].clone().expect_non_error()?.value;
    let value = arguments[2].clone().expect_non_error()?.value;

    if let Some(index) = map
        .values
        .iter()
        .position(|(found_key, _)| *found_key == key)
    {
        map.values.remove(index);
    }

    map.values.push((key, value));

    Ok((Value::new_ok_value(map.into(), span), None))
}

pub fn delete(
    arguments: &[ValueResult],
    span: Span,
) -> Result<(ValueResult, Option<ExitCode>), Error> {
    let arguments = arguments.to_vec();
    let arguments = arguments.expect_exact::<2>(&span)?;
    let mut map = arguments[0]
        .clone()
        .expect_non_error()?
        .expect::<values::Map>()?;
    let key = arguments[1].clone().expect_non_error()?.value;

    if let Some(index) = map
        .values
        .iter()
        .position(|(found_key, _)| *found_key == key)
    {
        map.values.remove(index);
    }

    Ok((Value::new_ok_value(map.into(), span), None))
}

pub fn get_module() -> Module {
    Module::builder()
        .function("insert", insert, false)
        .function("delete", delete, false)
        .build()
}
