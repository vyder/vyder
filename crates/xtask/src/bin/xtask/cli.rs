use clap::{Arg, Command};

pub fn build_cli() -> Command {
    Command::new(clap::crate_name!())
        .author(clap::crate_authors!())
        .version(clap::crate_version!())
        .about(clap::crate_description!())
        .subcommand_required(true)
        .subcommand(
            Command::new("git-hooks")
                .about("run git hooks")
                .subcommand_required(true)
                .subcommand(Command::new("pre-commit").about("pre-commit hook"))
                .subcommand(
                    Command::new("commit-msg")
                        .arg(
                            Arg::new("commit-message-path")
                                .required(true)
                                .help("The path to the file with the commit message."),
                        )
                        .about("commit-msg hook"),
                ),
        )
}
