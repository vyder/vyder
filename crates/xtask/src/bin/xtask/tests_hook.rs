use xtask::git_hooks::Hook;

pub struct TestsHook;

impl Hook for TestsHook {
    fn execute(&self, _work_dir: &str) -> Result<(), String> {
        std::process::Command::new("cargo")
            .arg("test")
            .output()
            .map_err(|_| "The hook failed to call the test command. Is cargo installed?")?
            .status
            .success()
            .then_some(())
            .ok_or("Some tests failed. Run 'cargo test' to see them.")?;

        Ok(())
    }

    fn short_success_message(&self) -> String {
        "all tests passed".to_string()
    }

    fn short_error_message(&self) -> String {
        "some tests failed".to_string()
    }
}
