use xtask::git_hooks::Hook;

pub struct FormatHook;

impl Hook for FormatHook {
    fn execute(&self, _work_dir: &str) -> Result<(), String> {
        std::process::Command::new("cargo")
            .arg("fmt")
            .arg("--check")
            .output()
            .map_err(|_| "The hook failed to call the format command. Is it installed?")?
            .status.success().then_some(()).ok_or("The code is not formatted correctly. Run 'cargo fmt --all' to automatically format everything.".to_string())?;

        Ok(())
    }

    fn short_success_message(&self) -> String {
        "code is formatted".to_string()
    }

    fn short_error_message(&self) -> String {
        "code is not formatted correctly".to_string()
    }
}
