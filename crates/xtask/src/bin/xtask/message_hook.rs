pub struct MessageHook;

const ALLOWED_COMMIT_TYPES: [&str; 10] = [
    "feat", "fix", "build", "chore", "ci", "docs", "style", "refactor", "perf", "test",
];

impl xtask::git_hooks::MessageHook for MessageHook {
    fn execute(&self, _work_dir: &str, commit_message: &str) -> Result<(), String> {
        let commit = conventional_commits_parser::parse_commit_msg(commit_message).map_err(|_| "Invalid commit message. The message has to follow 'Conventional Commits' convention.")?;

        if !ALLOWED_COMMIT_TYPES.contains(&commit.ty) {
            return Err(format!(
                r#"The commit type '{}' is not valid.
Valid types are 'feat', 'fix', 'build', 'chore', 'ci', 'docs', 'style', 'refactor', 'perf', 'test'"#,
                commit.ty,
            ));
        }

        Ok(())
    }

    fn short_success_message(&self) -> String {
        "commit message follows the conventions".to_string()
    }

    fn short_error_message(&self) -> String {
        "the commit message does not follow the convention".to_string()
    }
}
