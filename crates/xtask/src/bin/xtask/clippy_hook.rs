use xtask::git_hooks::Hook;

pub struct ClippyHook;

impl Hook for ClippyHook {
    fn execute(&self, _work_dir: &str) -> Result<(), String> {
        std::process::Command::new("cargo")
            .arg("clippy")
            .arg("--")
            .arg("-D")
            .arg("warnings")
            .output()
            .map_err(|_| "The hook failed to call the clippy command. Is it installed?")?
            .status
            .success()
            .then_some(())
            .ok_or("Clippy has some complaints. Run 'cargo clippy' to see them.")?;

        Ok(())
    }

    fn short_success_message(&self) -> String {
        "clippy is happy".to_string()
    }

    fn short_error_message(&self) -> String {
        "clippy is not happy".to_string()
    }
}
