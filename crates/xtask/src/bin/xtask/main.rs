use std::process::ExitCode;

use xtask::git_hooks::HookRunner;

mod cli;

mod clippy_hook;
use clippy_hook::ClippyHook;

mod format_hook;
use format_hook::FormatHook;

mod message_hook;
use message_hook::MessageHook;

mod tests_hook;
use tests_hook::TestsHook;

fn main() -> ExitCode {
    let matches = cli::build_cli().get_matches();

    match matches.subcommand() {
        Some(("git-hooks", matches)) => match matches.subcommand() {
            Some(("pre-commit", _)) => HookRunner::new(
                vec![
                    Box::new(ClippyHook),
                    Box::new(FormatHook),
                    Box::new(TestsHook),
                ],
                vec![],
            )
            .run(""),
            Some(("commit-msg", matches)) => HookRunner::new(vec![], vec![Box::new(MessageHook)])
                .run(matches.get_one::<String>("commit-message-path").unwrap()),
            _ => unreachable!(),
        },

        _ => unreachable!(),
    }
}
