use std::process::ExitCode;

use colored::Colorize;

use crate::git_hooks::{Hook, MessageHook};

pub struct HookRunner {
    hooks: Vec<Box<dyn Hook>>,
    message_hooks: Vec<Box<dyn MessageHook>>,
}

impl HookRunner {
    pub fn new(hooks: Vec<Box<dyn Hook>>, message_hooks: Vec<Box<dyn MessageHook>>) -> Self {
        Self {
            hooks,
            message_hooks,
        }
    }

    pub fn run(&self, commit_message_path: &str) -> ExitCode {
        let mut long_error_messages = vec![];

        for hook in &self.hooks {
            match hook.execute(&Self::get_work_dir()) {
                Ok(_) => Self::print_success(hook.short_success_message()),
                Err(long_error_message) => {
                    long_error_messages.push(long_error_message);
                    Self::print_error(hook.short_error_message());
                }
            }
        }

        if !self.message_hooks.is_empty() {
            let commit_message = std::fs::read_to_string(commit_message_path).unwrap();

            for hook in &self.message_hooks {
                match hook.execute(&Self::get_work_dir(), &commit_message) {
                    Ok(_) => Self::print_success(hook.short_success_message()),
                    Err(long_error_message) => {
                        long_error_messages.push(long_error_message);
                        Self::print_error(hook.short_error_message());
                    }
                }
            }
        }

        for long_error_message in &long_error_messages {
            println!();
            println!("{} {}", "-".red(), long_error_message.red());
        }

        if long_error_messages.is_empty() {
            ExitCode::SUCCESS
        } else {
            println!();
            println!("{}", "After fixing these issues, remember to add your changes to the commit using 'git add'".blue());
            ExitCode::FAILURE
        }
    }

    fn print_success(short_success_message: impl std::fmt::Display) {
        println!("{} {}", "✓".green(), short_success_message);
    }

    fn print_error(short_error_message: impl std::fmt::Display) {
        println!("{} {}", "⨯".red(), short_error_message);
    }

    fn get_work_dir() -> String {
        ".".to_string()
    }
}
