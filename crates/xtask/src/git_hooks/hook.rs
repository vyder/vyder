pub trait Hook {
    fn execute(&self, work_dir: &str) -> Result<(), String>;
    fn short_success_message(&self) -> String;
    fn short_error_message(&self) -> String;
}

pub trait MessageHook {
    fn execute(&self, work_dir: &str, commit_message: &str) -> Result<(), String>;
    fn short_success_message(&self) -> String;
    fn short_error_message(&self) -> String;
}
