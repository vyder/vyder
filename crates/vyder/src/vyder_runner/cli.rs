use clap::{Arg, Command, ValueHint};

pub fn build_cli() -> Command {
    Command::new("vyder")
        .author("Silvan Schmidt <mail@silvanschmidt.ch>")
        .version(clap::crate_version!())
        .about(clap::crate_description!())
        .subcommand_required(true)
        .subcommand(
            Command::new("run").about("run a vyder script").arg(
                Arg::new("file_path")
                    .required(true)
                    .value_hint(ValueHint::FilePath)
                    .help("path to the script to run"),
            ),
        )
        .subcommand(Command::new("repl").about("start a repl"))
}
