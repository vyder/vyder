use crate::Module;

/// A collection of `Module`'s.
pub struct Library {
    pub modules: Vec<(String, Module)>,
}

impl Library {
    /// Create a `LibraryBuilder` to help make a library.
    pub fn builder() -> LibraryBuilder {
        LibraryBuilder { modules: vec![] }
    }
}

/// Helper struct to create a `Library`.
pub struct LibraryBuilder {
    modules: Vec<(String, Module)>,
}

impl LibraryBuilder {
    /// Build a `Library` out of this `LibraryBuilder`.
    pub fn build(self) -> Library {
        Library {
            modules: self.modules,
        }
    }

    /// Add a `Module` to the library.
    pub fn module(mut self, identifier: &str, module: Module) -> Self {
        self.modules.push((identifier.to_string(), module));
        self
    }
}
