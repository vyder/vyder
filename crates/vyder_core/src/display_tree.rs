use crate::prelude::*;

pub fn display_expression(expression: &Expression) -> String {
    display_tree(expression.get_display_tree())
}

pub fn display_statement(statement: &Statement) -> String {
    display_tree(statement.get_display_tree())
}

fn display_tree(tree: Vec<(Location, String)>) -> String {
    tree.into_iter()
        .map(|(location, string)| format!("{: >8}  {}", location.to_string(), string))
        .collect::<Vec<String>>()
        .join("\n")
}

pub(crate) fn display_grouped(
    name: &str,
    span: Span,
    elements: Vec<(Location, String)>,
) -> Vec<(Location, String)> {
    let mut out = vec![(span.get_start(), format!("( {}", name))];

    for (location, expression) in elements {
        out.push((location, format!("  {}", expression)));
    }

    out.push((span.get_end(), ")".to_string()));

    out
}
