use crate::prelude::*;

pub const VALID_IDENTIFIER_CHARS: &str =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

#[derive(Clone, Debug, PartialEq)]
pub struct Identifier {
    name: String,
    span: Span,
    can_be_error: bool,
}

impl Identifier {
    pub fn new(name: &str, span: Span, can_be_error: bool) -> Result<Self> {
        for ch in name.chars() {
            if !VALID_IDENTIFIER_CHARS.contains(ch) {
                return Err(Error::new(ErrorType::Custom { message: format!("tried to construct an invalid identifier: '{}' cannot be used in an identifier", ch) }, span));
            }
        }

        Ok(Self {
            name: name.to_string(),
            span,
            can_be_error,
        })
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn span(&self) -> &Span {
        &self.span
    }

    pub fn can_be_error(&self) -> bool {
        self.can_be_error
    }
}

impl std::fmt::Display for Identifier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)?;
        if self.can_be_error {
            write!(f, "?")?;
        }

        Ok(())
    }
}
