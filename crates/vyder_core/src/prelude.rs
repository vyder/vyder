pub use crate::{
    expressions, statements, tokens, values, ControlFlow, Environment, Error, ErrorType,
    ExpectExpression, ExpectStatement, ExpectToken, Expression, ExpressionEnum, IntoExpressionEnum,
    IntoStatementEnum, IntoTokenEnum, IntoValueEnum, Location, Parser, Span, Statement,
    StatementEnum, Token, TokenEnum, Value, ValueEnum, ValueResult,
};

pub type Result<T, E = Error> = std::result::Result<T, E>;

pub trait UnwrapFmt<T> {
    fn unwrap_fmt(self) -> T;
}

impl<T> UnwrapFmt<T> for Result<T, Error> {
    fn unwrap_fmt(self) -> T {
        match self {
            Ok(value) => value,
            Err(e) => {
                panic!("{}", e);
            }
        }
    }
}
