use colored::Colorize;

use crate::prelude::*;

/// Vyder's error type.
#[derive(Debug)]
pub struct Error {
    // This is boxed to reduce the size of the error (https://rust-lang.github.io/rust-clippy/master/index.html#/result_large_err).
    pub ty: Box<ErrorType>,
    pub span: Span,
}

impl Error {
    /// Create a new error.
    pub fn new(ty: ErrorType, span: Span) -> Self {
        Self {
            ty: Box::new(ty),
            span,
        }
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", "error".red())?;
        write!(f, " at {}\n{}", self.span, self.ty.to_string().red())?;
        Ok(())
    }
}

impl std::error::Error for Error {}

/// The different variants of errors.
#[derive(Debug)]
pub enum ErrorType {
    DivisionByZero,
    ModuloByZero,
    /// Used when the interpreter encounters an unexpected statement.
    UnexpectedStatement {
        expected: String,
        actual: String,
    },
    /// Used when the parser expected an expression.
    ExpectedExpression,
    /// Used when the parser expected an expression.
    ExpectedStatement,
    /// Used when the interpreter encounters an unexpected expression.
    UnexpectedExpression {
        expected: String,
        actual: String,
    },
    /// Used when the parser encounters an unexpected token.
    UnexpectedToken {
        expected: Vec<String>,
        actual: String,
    },
    /// Used when the parser encounters an unexpected value.
    UnexpectedValue {
        expected: Vec<String>,
        actual: String,
    },
    ExpectedNonErrorValue,
    UnexpectedChar {
        unexpected_char: char,
    },
    /// Invalid escape sequence.
    InvalidEscapeSequence,
    /// Used when encountering a string that is not delimited.
    UnfinishedString,
    InvalidKey(String),
    UndefinedVariable(String),
    CannotLoopOver(String),
    CannotAssignToConstant(String),
    UnexpectedAmountOfArguments {
        expected: usize,
        actual: usize,
    },
    NotEnoughArguments {
        expected: usize,
        actual: usize,
    },
    ModuleNotFound(String),
    /// Used for errors that are not common enough to have their own enum variant.
    Custom {
        message: String,
    },
}

impl std::fmt::Display for ErrorType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ErrorType::DivisionByZero => write!(f, "division by zero"),
            ErrorType::ModuloByZero => write!(f, "modulo by zero"),
            ErrorType::UnexpectedStatement { expected, actual } => {
                write!(
                    f,
                    "unexpected statement: expected {}, got {}",
                    expected, actual
                )
            }
            ErrorType::ExpectedExpression => write!(f, "expected expression"),
            ErrorType::ExpectedStatement => write!(f, "expected statement"),
            ErrorType::UnexpectedExpression { expected, actual } => {
                write!(
                    f,
                    "unexpected expression: expected {}, got {}",
                    expected, actual
                )
            }
            ErrorType::UnexpectedToken { expected, actual } => match expected.len() {
                0 => write!(f, "unexpected token: got {}", actual),
                1 => write!(
                    f,
                    "unexpected token: expected {}, got {}",
                    expected.first().unwrap(),
                    actual
                ),
                _ => write!(
                    f,
                    "unexpected token: expected either {}, got {}",
                    {
                        let mut expected_string = String::new();
                        for string in expected.iter().take(expected.len() - 1) {
                            expected_string.push_str(&format!("{}, ", string));
                        }

                        expected_string.pop();
                        expected_string.pop();

                        expected_string.push_str(&format!(" or {}", expected.last().unwrap()));

                        expected_string
                    },
                    actual
                ),
            },
            ErrorType::UnexpectedValue { expected, actual } => match expected.len() {
                0 => write!(f, "unexpected value: got {}", actual),
                1 => write!(
                    f,
                    "unexpected value: expected {}, got {}",
                    expected.first().unwrap(),
                    actual
                ),
                _ => write!(
                    f,
                    "unexpected value: expected either {}, got {}",
                    {
                        let mut expected_string = String::new();
                        for string in expected.iter().take(expected.len() - 1) {
                            expected_string.push_str(&format!("{}, ", string));
                        }

                        expected_string.pop();
                        expected_string.pop();

                        expected_string.push_str(&format!(" or {}", expected.last().unwrap()));

                        expected_string
                    },
                    actual
                ),
            },
            ErrorType::ExpectedNonErrorValue => write!(f, "expected a non-error value"),
            ErrorType::UnexpectedChar { unexpected_char } => {
                write!(f, "unexpected char '{}'", unexpected_char)
            }
            ErrorType::UnfinishedString => write!(f, "unfinished string"),
            ErrorType::InvalidEscapeSequence => write!(f, "invalid escape sequence"),
            ErrorType::InvalidKey(key) => write!(f, "no value with key '{}' in map", key),
            ErrorType::UndefinedVariable(identifier) => {
                write!(f, "tried to access an undefined variable '{}'", identifier)
            }
            ErrorType::CannotLoopOver(value) => {
                write!(f, "cannot loop over value of type '{}'", value)
            }
            ErrorType::UnexpectedAmountOfArguments { expected, actual } => {
                write!(f, "expected {} argument(s), got {}", expected, actual)
            }
            ErrorType::CannotAssignToConstant(identifier) => write!(
                f,
                "'{}' is defined as a constant, so it cannot be re-assigned",
                identifier
            ),
            ErrorType::NotEnoughArguments { expected, actual } => {
                write!(
                    f,
                    "not enough arguments. expected at least {}, found {}",
                    expected, actual
                )
            }
            ErrorType::ModuleNotFound(name) => {
                write!(f, "no module with the name '{}' found", name)
            }
            ErrorType::Custom { message } => write!(f, "{}", message),
        }
    }
}
