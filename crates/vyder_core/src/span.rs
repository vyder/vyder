use colored::Colorize;

#[derive(Clone, Debug)]
pub struct Span {
    start_location: Location,
    end_location: Option<Location>,

    module_name: String,
    snippet: Option<String>,
}

impl Span {
    pub fn new(location: impl Into<Location>, module_name: impl Into<String>) -> Self {
        Self {
            start_location: location.into(),
            end_location: None,
            module_name: module_name.into(),
            snippet: None,
        }
    }

    pub fn new_with_end(
        location: impl Into<Location>,
        end_location: impl Into<Location>,
        module_name: impl Into<String>,
    ) -> Self {
        Self {
            start_location: location.into(),
            end_location: Some(end_location.into()),
            module_name: module_name.into(),
            snippet: None,
        }
    }

    pub fn combine(start: &Span, end: &Span) -> Self {
        let mut snippet = &start.snippet;
        if snippet.is_none() {
            snippet = &end.snippet;
        };

        Self {
            start_location: start.get_start(),
            end_location: Some(end.get_end()),
            module_name: start.module_name.clone(),
            snippet: snippet.clone(),
        }
    }

    pub fn get_length(&self) -> usize {
        self.get_end().char_index - self.get_start().char_index + 1
    }

    pub fn add_snippet(mut self, snippet: impl Into<String>) -> Self {
        self.snippet = Some(snippet.into());
        self
    }

    pub fn set_snippet(mut self, snippet: Option<String>) -> Self {
        self.snippet = snippet;
        self
    }

    pub fn get_snippet(&self) -> &Option<String> {
        &self.snippet
    }

    pub fn set_end(&mut self, location: Location) {
        if location == self.start_location {
            self.end_location = None;
        } else {
            self.end_location = Some(location);
        }
    }

    pub fn get_start(&self) -> Location {
        self.start_location
    }

    pub fn get_end(&self) -> Location {
        match self.end_location {
            Some(end_location) => end_location,
            None => self.start_location,
        }
    }
}

impl PartialEq for Span {
    fn eq(&self, other: &Self) -> bool {
        self.start_location == other.start_location
            && self.end_location == other.end_location
            && self.module_name == other.module_name // ignore snippet for comparison
    }
}

impl std::fmt::Display for Span {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self.end_location {
            Some(end_location) => {
                write!(
                    f,
                    "{} {} - {} ({} chars)",
                    self.module_name,
                    self.start_location.to_string().blue(),
                    end_location.to_string().blue(),
                    self.get_length()
                )?;
            }
            None => {
                write!(
                    f,
                    "{} {}",
                    self.module_name,
                    self.start_location.to_string().blue()
                )?;
            }
        }

        if let Some(snippet) = &self.snippet {
            write!(f, "\n{} {}\n", "|".blue(), snippet.replace('\t', "    "))?;

            let mut spacer = String::new();
            for _ in 0..self.start_location.column + 1 {
                spacer.push(' ')
            }
            write!(f, "{}", spacer)?;

            match self.end_location {
                Some(_) => {
                    for _ in 0..self.get_length() {
                        write!(f, "{}", "^".green())?;
                    }
                    write!(f, " {}", "here".green())?;
                }
                None => write!(f, "\n{}{}", spacer, "^ here".green())?,
            };
        }

        Ok(())
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Location {
    pub line: usize,
    pub column: usize,
    pub char_index: usize,
}

impl Location {
    pub fn new(line: usize, column: usize, char_index: usize) -> Self {
        Self {
            line,
            column,
            char_index,
        }
    }
}

impl From<(usize, usize, usize)> for Location {
    fn from(value: (usize, usize, usize)) -> Self {
        Location {
            line: value.0,
            column: value.1,
            char_index: value.2,
        }
    }
}

impl std::fmt::Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}:{}", self.line, self.column)
    }
}
