use enum_downcast::EnumDowncast;

use crate::display_tree::display_grouped;
use crate::prelude::*;

pub mod expressions;

/// The expression type.
#[derive(Clone, Debug, PartialEq)]
pub struct Expression {
    /// The type of expression.
    pub value: ExpressionEnum,
    /// The location of the expression.
    pub span: Span,
}

impl Expression {
    /**
    Downcast this expression to its inner type.
    Returns an ErrorType::UnexpectedExpression when T is not the type of the inner expression.

    # Examples

    ```rust
    use vyder_core::prelude::*;

    let expression: Expression = Expression { value: expressions::Number { value: 0.0 }.into(), span: Span::new((1, 1, 0), "example") };
    assert!(expression.expect::<expressions::False>().is_err());
    let number_expression: expressions::Number = expression.expect::<expressions::Number>().unwrap();
    assert_eq!(number_expression.value, 0.0);
    ```
    **/
    pub fn expect<T>(&self) -> Result<T>
    where
        ExpressionEnum: enum_downcast::AsVariant<T>,
        T: Clone,
    {
        match self.value.enum_downcast_ref::<T>() {
            Some(expression) => Ok(expression.clone()),
            None => Err(Error::new(
                {
                    let expected = std::any::type_name::<T>().split("::").last().unwrap();
                    let actual = self.value.name();

                    ErrorType::UnexpectedExpression {
                        expected: expected.to_string(),
                        actual,
                    }
                },
                self.span.clone(),
            )),
        }
    }

    pub(crate) fn get_display_tree(&self) -> Vec<(Location, String)> {
        use ExpressionEnum::*;

        match &self.value {
            Number(number) => vec![(self.span.get_start(), number.value.to_string())],
            Str(string) => vec![(self.span.get_start(), format!("\"{}\"", string.value))],
            Identifier(identifier) => {
                vec![(self.span.get_start(), identifier.value.to_string())]
            }
            False(_) => vec![(self.span.get_start(), "false".to_string())],
            True(_) => vec![(self.span.get_start(), "true".to_string())],
            Nil(_) => vec![(self.span.get_start(), "nil".to_string())],
            Group(group) => {
                display_grouped("group", self.span.clone(), group.value.get_display_tree())
            }
            Block(block) => display_grouped(
                "block",
                self.span.clone(),
                block
                    .statements
                    .iter()
                    .flat_map(|stmt| stmt.get_display_tree())
                    .collect(),
            ),
            While(while_expression) => display_grouped(
                &format!(
                    "while{}",
                    if while_expression.can_be_error {
                        "?"
                    } else {
                        ""
                    },
                ),
                self.span.clone(),
                while_expression
                    .condition
                    .get_display_tree()
                    .into_iter()
                    .chain(while_expression.body.get_display_tree())
                    .collect(),
            ),
            If(if_expression) => display_grouped(
                &format!("if{}", if if_expression.can_be_error { "?" } else { "" }),
                self.span.clone(),
                {
                    let mut branches_iter = if_expression.branches.clone().into_iter();

                    let first_branch = branches_iter.next().unwrap();

                    let mut branches = display_grouped(
                        "branch",
                        first_branch.0.span.clone(),
                        first_branch
                            .0
                            .get_display_tree()
                            .into_iter()
                            .chain(first_branch.1.get_display_tree())
                            .collect(),
                    );

                    for (condition, body) in branches_iter {
                        branches.append(&mut display_grouped(
                            "branch",
                            condition.span.clone(),
                            condition
                                .get_display_tree()
                                .into_iter()
                                .chain(body.get_display_tree())
                                .collect(),
                        ));
                    }

                    if let Some(else_branch) = &if_expression.else_branch {
                        branches.append(&mut display_grouped(
                            "else",
                            else_branch.span.clone(),
                            else_branch.get_display_tree(),
                        ));
                    }

                    branches
                },
            ),
            Map(map) => display_grouped("map", self.span.clone(), {
                let mut out: Vec<(Location, String)> = vec![];

                for (key, value) in &map.expression_value {
                    out.append(&mut display_grouped(
                        "expression-value pair",
                        key.span.clone(),
                        key.get_display_tree()
                            .into_iter()
                            .chain(value.get_display_tree())
                            .collect(),
                    ));
                }

                for (identifier, value) in &map.identifier_value {
                    out.append(&mut display_grouped(
                        "identifier-value pair",
                        Span::combine(identifier.span(), &value.span),
                        vec![(identifier.span().get_start(), identifier.name().to_string())]
                            .into_iter()
                            .chain(value.get_display_tree())
                            .collect(),
                    ));
                }

                for identifier in &map.same_name_identifier {
                    out.append(&mut display_grouped(
                        "same-name-identifier pair",
                        identifier.span().clone(),
                        vec![(identifier.span().get_start(), identifier.name().to_string())],
                    ));
                }

                out
            }),
            Function(function) => display_grouped(
                &format!("function{}", if function.can_be_error { "?" } else { "" }),
                self.span.clone(),
                display_grouped(
                    "parameters",
                    self.span.clone(),
                    function
                        .parameters
                        .iter()
                        .map(|parameter| {
                            (
                                parameter.span().get_start(),
                                format!(
                                    "{}{}",
                                    parameter.name(),
                                    if parameter.can_be_error() { "?" } else { "" }
                                ),
                            )
                        })
                        .collect(),
                )
                .into_iter()
                .chain(function.body.get_display_tree())
                .collect(),
            ),
            Check(check) => display_grouped(
                &format!("check{}", if check.can_be_error { "?" } else { "" }),
                self.span.clone(),
                {
                    let mut out: Vec<(Location, String)> = vec![];

                    out.append(&mut check.expression_to_check.get_display_tree());

                    if let Some(alias) = &check.alias {
                        out.push((alias.span().get_start(), format!("as {}", alias.name(),)));
                    }

                    out.append(&mut check.body.get_display_tree());

                    if let Some(else_body) = &check.else_body {
                        if let Some(alias) = &check.else_alias {
                            out.push((alias.span().get_start(), format!("as {}", alias.name())))
                        }
                        out.append(&mut else_body.get_display_tree());
                    }

                    out
                },
            ),
            For(for_expression) => display_grouped(
                &format!("for{}", if for_expression.can_be_error { "?" } else { "" },),
                self.span.clone(),
                [(
                    for_expression.identifier.span().get_start(),
                    format!(
                        "{}{}",
                        for_expression.identifier.name(),
                        if for_expression.identifier.can_be_error() {
                            "?"
                        } else {
                            ""
                        }
                    ),
                )]
                .into_iter()
                .chain(for_expression.to_loop_over.get_display_tree())
                .chain(for_expression.body.get_display_tree())
                .collect(),
            ),
            Assignment(assignment) => display_grouped(
                "assign",
                self.span.clone(),
                vec![(
                    assignment.identifier.span().get_start(),
                    assignment.identifier.name().to_string(),
                )]
                .into_iter()
                .chain(assignment.expression.get_display_tree())
                .collect(),
            ),
            AdditionAssignment(assignment) => display_grouped(
                "add and assign",
                self.span.clone(),
                vec![(
                    assignment.identifier.span().get_start(),
                    assignment.identifier.name().to_string(),
                )]
                .into_iter()
                .chain(assignment.expression.get_display_tree())
                .collect(),
            ),
            SubtractionAssignment(assignment) => display_grouped(
                "subtract and assign",
                self.span.clone(),
                vec![(
                    assignment.identifier.span().get_start(),
                    assignment.identifier.name().to_string(),
                )]
                .into_iter()
                .chain(assignment.expression.get_display_tree())
                .collect(),
            ),
            MultiplicationAssignment(assignment) => display_grouped(
                "multiply and assign",
                self.span.clone(),
                vec![(
                    assignment.identifier.span().get_start(),
                    assignment.identifier.name().to_string(),
                )]
                .into_iter()
                .chain(assignment.expression.get_display_tree())
                .collect(),
            ),
            DivisionAssignment(assignment) => display_grouped(
                "divide and assign",
                self.span.clone(),
                vec![(
                    assignment.identifier.span().get_start(),
                    assignment.identifier.name().to_string(),
                )]
                .into_iter()
                .chain(assignment.expression.get_display_tree())
                .collect(),
            ),
            ModuloAssignment(assignment) => display_grouped(
                "modulo and assign",
                self.span.clone(),
                vec![(
                    assignment.identifier.span().get_start(),
                    assignment.identifier.name().to_string(),
                )]
                .into_iter()
                .chain(assignment.expression.get_display_tree())
                .collect(),
            ),
            And(and) => display_grouped(
                "and",
                self.span.clone(),
                and.lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(and.rhs.get_display_tree())
                    .collect(),
            ),
            Or(or) => display_grouped(
                "or",
                self.span.clone(),
                or.lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(or.rhs.get_display_tree())
                    .collect(),
            ),
            Equal(equal) => display_grouped(
                "equals",
                self.span.clone(),
                equal
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(equal.rhs.get_display_tree())
                    .collect(),
            ),
            NotEqual(not_equal) => display_grouped(
                "not equals",
                self.span.clone(),
                not_equal
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(not_equal.rhs.get_display_tree())
                    .collect(),
            ),
            Greater(greater) => display_grouped(
                "greater",
                self.span.clone(),
                greater
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(greater.rhs.get_display_tree())
                    .collect(),
            ),
            GreaterEqual(greater_equal) => display_grouped(
                "greater equal",
                self.span.clone(),
                greater_equal
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(greater_equal.rhs.get_display_tree())
                    .collect(),
            ),
            Less(less) => display_grouped(
                "less",
                self.span.clone(),
                less.lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(less.rhs.get_display_tree())
                    .collect(),
            ),
            LessEqual(less_equal) => display_grouped(
                "less equal",
                self.span.clone(),
                less_equal
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(less_equal.rhs.get_display_tree())
                    .collect(),
            ),
            Range(range) => match &range {
                expressions::Range::Closed { rhs, lhs } => display_grouped(
                    "closed range",
                    self.span.clone(),
                    lhs.get_display_tree()
                        .into_iter()
                        .chain(rhs.get_display_tree())
                        .collect(),
                ),
                expressions::Range::From { lhs } => {
                    display_grouped("range from n", self.span.clone(), lhs.get_display_tree())
                }
                expressions::Range::To { rhs } => {
                    display_grouped("range to n", self.span.clone(), rhs.get_display_tree())
                }
            },
            Addition(addition) => display_grouped(
                "add",
                self.span.clone(),
                addition
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(addition.rhs.get_display_tree())
                    .collect(),
            ),
            Subtraction(subtraction) => display_grouped(
                "subtract",
                self.span.clone(),
                subtraction
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(subtraction.rhs.get_display_tree())
                    .collect(),
            ),
            Multiplication(multiplication) => display_grouped(
                "multiply",
                self.span.clone(),
                multiplication
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(multiplication.rhs.get_display_tree())
                    .collect(),
            ),
            Division(division) => display_grouped(
                "divide",
                self.span.clone(),
                division
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(division.rhs.get_display_tree())
                    .collect(),
            ),
            Modulo(modulo) => display_grouped(
                "modulo",
                self.span.clone(),
                modulo
                    .lhs
                    .get_display_tree()
                    .into_iter()
                    .chain(modulo.rhs.get_display_tree())
                    .collect(),
            ),
            AsError(as_error) => display_grouped(
                "as error",
                self.span.clone(),
                as_error.expression.get_display_tree(),
            ),
            Expect(expect) => display_grouped(
                "expect",
                self.span.clone(),
                expect.expression.get_display_tree(),
            ),
            FieldAccess(field_access) => display_grouped(
                "field access",
                self.span.clone(),
                field_access
                    .expression
                    .get_display_tree()
                    .into_iter()
                    .chain([(
                        field_access.field.span().get_start(),
                        format!(".{}", field_access.field.name()),
                    )])
                    .collect(),
            ),
            Index(index) => display_grouped(
                "index",
                self.span.clone(),
                index
                    .expression
                    .get_display_tree()
                    .into_iter()
                    .chain(index.index.get_display_tree())
                    .collect(),
            ),
            Call(call) => {
                if call.arguments.is_empty() {
                    display_grouped(
                        "call",
                        self.span.clone(),
                        call.expression.get_display_tree(),
                    )
                } else {
                    display_grouped(
                        "call",
                        self.span.clone(),
                        call.expression
                            .get_display_tree()
                            .into_iter()
                            .chain(display_grouped(
                                "arguments",
                                call.arguments
                                    .first()
                                    .expect("has at least one argument")
                                    .span
                                    .clone(),
                                call.arguments
                                    .clone()
                                    .into_iter()
                                    .flat_map(|expr| expr.get_display_tree())
                                    .collect(),
                            ))
                            .collect(),
                    )
                }
            }
            Not(not) => {
                display_grouped("not", self.span.clone(), not.expression.get_display_tree())
            }
            Negate(negate) => display_grouped(
                "negate",
                self.span.clone(),
                negate.expression.get_display_tree(),
            ),
        }
    }
}

pub trait ExpectExpression {
    fn expect_expression(self, span: &Span) -> Result<Expression>;
}

impl ExpectExpression for Option<Expression> {
    fn expect_expression(self, span: &Span) -> Result<Expression> {
        match self {
            Some(expression) => Ok(expression),
            None => Err(Error::new(ErrorType::ExpectedExpression, span.clone())),
        }
    }
}

impl std::fmt::Display for Expression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "'{}' at {}", self.value, self.span)
    }
}

/// Enum of all expressions.
#[derive(Clone, Debug, EnumDowncast, PartialEq)]
pub enum ExpressionEnum {
    Number(expressions::Number),
    Str(expressions::Str),
    Identifier(expressions::Identifier),
    False(expressions::False),
    True(expressions::True),
    Nil(expressions::Nil),
    Group(expressions::Group),
    Map(expressions::Map),
    Block(expressions::Block),
    If(expressions::If),
    While(expressions::While),
    Function(expressions::Function),
    Check(expressions::Check),
    For(expressions::For),
    Assignment(expressions::Assignment),
    AdditionAssignment(expressions::AdditionAssignment),
    SubtractionAssignment(expressions::SubtractionAssignment),
    MultiplicationAssignment(expressions::MultiplicationAssignment),
    DivisionAssignment(expressions::DivisionAssignment),
    ModuloAssignment(expressions::ModuloAssignment),
    And(expressions::And),
    Or(expressions::Or),
    Equal(expressions::Equal),
    NotEqual(expressions::NotEqual),
    Greater(expressions::Greater),
    GreaterEqual(expressions::GreaterEqual),
    Less(expressions::Less),
    LessEqual(expressions::LessEqual),
    Range(expressions::Range),
    Addition(expressions::Addition),
    Subtraction(expressions::Subtraction),
    Multiplication(expressions::Multiplication),
    Division(expressions::Division),
    Modulo(expressions::Modulo),
    Not(expressions::Not),
    Negate(expressions::Negate),
    AsError(expressions::AsError),
    Expect(expressions::Expect),
    FieldAccess(expressions::FieldAccess),
    Index(expressions::Index),
    Call(expressions::Call),
}

impl ExpressionEnum {
    /// Get the name of the expression for debugging purposes.
    pub fn name(&self) -> String {
        use ExpressionEnum::*;

        match self {
            Number(_) => "Number".to_string(),
            Str(_) => "Str".to_string(),
            Identifier(_) => "Identifier".to_string(),
            Group(_) => "Group".to_string(),
            Block(_) => "Block".to_string(),
            _ => self.to_string(),
        }
    }
}

impl std::fmt::Display for ExpressionEnum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use ExpressionEnum::*;

        write!(
            f,
            "{}",
            match self {
                Number(number) => number.value.to_string(),
                Str(string) => format!("\"{}\"", string.value),
                Identifier(identifier) => identifier.value.clone(),
                False(_) => "false".to_string(),
                True(_) => "true".to_string(),
                Nil(_) => "nil".to_string(),
                Group(group) => format!("({})", group.value),
                Map(_) => "Map".to_string(),
                Block(block) => {
                    format!("{{\n{}\n}}", {
                        let mut out = String::new();

                        for statement in &block.statements {
                            out.push_str(&format!("  {};\n", statement))
                        }

                        out
                    })
                }
                If(_) => "If".to_string(),
                While(_) => "While".to_string(),
                Function(_) => "Function".to_string(),
                Check(_) => "Check".to_string(),
                For(_) => "For".to_string(),
                Assignment(_) => "Assignment".to_string(),
                AdditionAssignment(_) => "AdditionAssignment".to_string(),
                SubtractionAssignment(_) => "SubtractionAssignment".to_string(),
                MultiplicationAssignment(_) => "MultiplicationAssignment".to_string(),
                DivisionAssignment(_) => "DivisionAssignment".to_string(),
                ModuloAssignment(_) => "ModuloAssignment".to_string(),
                And(_) => "And".to_string(),
                Or(_) => "Or".to_string(),
                Equal(_) => "Equal".to_string(),
                NotEqual(_) => "NotEqual".to_string(),
                Greater(_) => "Greater".to_string(),
                GreaterEqual(_) => "GreaterEqual".to_string(),
                Less(_) => "Less".to_string(),
                LessEqual(_) => "LessEqual".to_string(),
                Range(_) => "Range".to_string(),
                Addition(_) => "Addition".to_string(),
                Subtraction(_) => "Subtraction".to_string(),
                Multiplication(_) => "Multiplication".to_string(),
                Division(_) => "Division".to_string(),
                Modulo(_) => "Modulo".to_string(),
                Not(_) => "Not".to_string(),
                Negate(_) => "Negate".to_string(),
                AsError(_) => "AsError".to_string(),
                Expect(_) => "Expect".to_string(),
                FieldAccess(_) => "FieldAccess".to_string(),
                Index(_) => "Index".to_string(),
                Call(call) => format!("Call({})", call.expression.value),
            }
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn expect_test() {
        let expression = Expression {
            value: expressions::Number { value: 0.0 }.into(),
            span: Span::new((1, 1, 0), "test"),
        };

        assert!(expression.expect::<expressions::False>().is_err());

        let number_expression = expression.expect::<expressions::Number>().unwrap();
        assert_eq!(number_expression.value, 0.0);
    }
}
