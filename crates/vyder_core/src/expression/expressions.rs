use crate::prelude::*;

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Number {
    pub value: f64,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Str {
    pub value: String,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Identifier {
    pub value: String,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct False;

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct True;

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Nil;

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Group {
    pub value: Box<Expression>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Map {
    // [2 + 3] = 10
    pub expression_value: Vec<(Expression, Expression)>,
    // foo = 10 (=> ["foo"] = 10)
    pub identifier_value: Vec<(crate::Identifier, Expression)>,
    // foo (=> ["foo"] = foo)
    pub same_name_identifier: Vec<crate::Identifier>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Block {
    pub statements: Vec<Statement>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct If {
    pub can_be_error: bool,
    /// 0: condition, 1: body
    pub branches: Vec<(Expression, Expression)>,
    pub else_branch: Option<Box<Expression>>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct While {
    pub can_be_error: bool,
    pub condition: Box<Expression>,
    pub body: Box<Expression>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Function {
    pub can_be_error: bool,
    pub parameters: Vec<crate::Identifier>,
    pub body: Box<Expression>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Check {
    pub can_be_error: bool,
    pub expression_to_check: Box<Expression>,
    pub alias: Option<crate::Identifier>,
    pub body: Box<Expression>,
    pub else_body: Option<Box<Expression>>,
    pub else_alias: Option<crate::Identifier>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct For {
    pub can_be_error: bool,
    pub identifier: crate::Identifier,
    pub to_loop_over: Box<Expression>,
    pub body: Box<Expression>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Assignment {
    pub identifier: crate::Identifier,
    pub expression: Box<Expression>,
}

impl Assignment {
    pub fn new_as_expression(
        identifier: crate::Identifier,
        expression: Expression,
        span: Span,
    ) -> Expression {
        Expression {
            span,
            value: Self {
                identifier,
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct AdditionAssignment {
    pub identifier: crate::Identifier,
    pub expression: Box<Expression>,
}

impl AdditionAssignment {
    pub fn new_as_expression(
        identifier: crate::Identifier,
        expression: Expression,
        span: Span,
    ) -> Expression {
        Expression {
            span,
            value: Self {
                identifier,
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct SubtractionAssignment {
    pub identifier: crate::Identifier,
    pub expression: Box<Expression>,
}

impl SubtractionAssignment {
    pub fn new_as_expression(
        identifier: crate::Identifier,
        expression: Expression,
        span: Span,
    ) -> Expression {
        Expression {
            span,
            value: Self {
                identifier,
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct MultiplicationAssignment {
    pub identifier: crate::Identifier,
    pub expression: Box<Expression>,
}

impl MultiplicationAssignment {
    pub fn new_as_expression(
        identifier: crate::Identifier,
        expression: Expression,
        span: Span,
    ) -> Expression {
        Expression {
            span,
            value: Self {
                identifier,
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct DivisionAssignment {
    pub identifier: crate::Identifier,
    pub expression: Box<Expression>,
}

impl DivisionAssignment {
    pub fn new_as_expression(
        identifier: crate::Identifier,
        expression: Expression,
        span: Span,
    ) -> Expression {
        Expression {
            span,
            value: Self {
                identifier,
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct ModuloAssignment {
    pub identifier: crate::Identifier,
    pub expression: Box<Expression>,
}

impl ModuloAssignment {
    pub fn new_as_expression(
        identifier: crate::Identifier,
        expression: Expression,
        span: Span,
    ) -> Expression {
        Expression {
            span,
            value: Self {
                identifier,
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct And {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl And {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Or {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Or {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Equal {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Equal {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}
#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct NotEqual {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl NotEqual {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Greater {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Greater {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct GreaterEqual {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl GreaterEqual {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Less {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Less {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct LessEqual {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl LessEqual {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub enum Range {
    Closed {
        rhs: Box<Expression>,
        lhs: Box<Expression>,
    },
    From {
        lhs: Box<Expression>,
    },
    To {
        rhs: Box<Expression>,
    },
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Addition {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Addition {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Subtraction {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Subtraction {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Multiplication {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Multiplication {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Division {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Division {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Modulo {
    pub lhs: Box<Expression>,
    pub rhs: Box<Expression>,
}

impl Modulo {
    pub fn new_as_expression(lhs: Expression, rhs: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Not {
    pub expression: Box<Expression>,
}

impl Not {
    pub fn new_as_expression(expression: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Negate {
    pub expression: Box<Expression>,
}

impl Negate {
    pub fn new_as_expression(expression: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

/// ~value
#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct AsError {
    pub expression: Box<Expression>,
}

impl AsError {
    pub fn new_as_expression(expression: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

/// ?value
#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Expect {
    pub expression: Box<Expression>,
}

impl Expect {
    pub fn new_as_expression(expression: Expression, span: Span) -> Expression {
        Expression {
            span,
            value: Self {
                expression: Box::new(expression),
            }
            .into(),
        }
    }
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct FieldAccess {
    pub expression: Box<Expression>,
    pub field: crate::Identifier,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Index {
    pub expression: Box<Expression>,
    pub index: Box<Expression>,
}

#[derive(Clone, Debug, IntoExpressionEnum, PartialEq)]
pub struct Call {
    pub expression: Box<Expression>,
    pub arguments: Vec<Expression>,
}
