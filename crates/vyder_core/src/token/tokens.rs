use crate::prelude::*;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Number {
    pub value: f64,
}

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Str {
    pub value: String,
}

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Identifier {
    pub value: String,
}

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct LeftParen;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct RightParen;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct LeftBrace;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct RightBrace;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct LeftBracket;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct RightBracket;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Pipe;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Comma;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Dot;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Colon;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Semicolon;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct DotDot;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Plus;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Minus;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Star;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Slash;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Percent;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct PlusEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct MinusEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct StarEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct SlashEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct PercentEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct EqualEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct BangEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Greater;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct GreaterEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Less;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct LessEqual;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Bang;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Tilde;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct QuestionMark;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct False;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct True;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Nil;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct If;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Else;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Check;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct As;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct And;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Or;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Not;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Function;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Return;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Ev;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Break;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Continue;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct For;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct While;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct In;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Let;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Const;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Equal;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct Exit;

#[derive(Clone, Debug, IntoTokenEnum, PartialEq)]
pub struct EOF;
