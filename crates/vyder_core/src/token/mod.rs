use enum_downcast::EnumDowncast;

use crate::prelude::*;

pub mod tokens;

/// The token type.
#[derive(Clone, Debug, PartialEq)]
pub struct Token {
    /// The type of token.
    pub value: TokenEnum,
    /// The location of the token.
    pub span: Span,
}

impl Token {
    /**
    Downcast this token to its inner type.
    Returns an ErrorType::UnexpectedToken when T is not the type of the inner token.

    # Examples

    ```rust
    use vyder_core::prelude::*;

    let token: Token = Token { value: tokens::Number { value: 0.0 }.into(), span: Span::new((1, 1, 0), "example") };
    assert!(token.expect::<tokens::Return>().is_err());
    let number_token: tokens::Number = token.expect::<tokens::Number>().unwrap();
    assert_eq!(number_token.value, 0.0);
    ```
    **/
    pub fn expect<T>(&self) -> Result<T>
    where
        TokenEnum: enum_downcast::AsVariant<T>,
        T: Clone,
    {
        match self.value.enum_downcast_ref::<T>() {
            Some(token) => Ok(token.clone()),
            None => Err(Error::new(
                {
                    let expected = std::any::type_name::<T>().split("::").last().unwrap();
                    let actual = self.value.name();

                    ErrorType::UnexpectedToken {
                        expected: vec![expected.to_string()],
                        actual,
                    }
                },
                self.span.clone(),
            )),
        }
    }
}

pub trait ExpectToken {
    fn expect_token(self, expected: &[&str], span: &Span) -> Result<Token>;
}

impl ExpectToken for Option<&Token> {
    fn expect_token(self, expected: &[&str], span: &Span) -> Result<Token> {
        match self {
            Some(token) => Ok(token.clone()),
            None => Err(Error::new(
                ErrorType::UnexpectedToken {
                    expected: expected.iter().map(|v| v.to_string()).collect(),
                    actual: "EOF".to_string(),
                },
                span.clone(),
            )),
        }
    }
}

impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "'{}' at {}", self.value, self.span)
    }
}

/// Enum of all tokens.
#[derive(Clone, Debug, EnumDowncast, PartialEq)]
pub enum TokenEnum {
    Number(tokens::Number),
    Str(tokens::Str),
    Identifier(tokens::Identifier),
    LeftParen(tokens::LeftParen),
    RightParen(tokens::RightParen),
    LeftBrace(tokens::LeftBrace),
    RightBrace(tokens::RightBrace),
    LeftBracket(tokens::LeftBracket),
    RightBracket(tokens::RightBracket),
    Pipe(tokens::Pipe),
    Comma(tokens::Comma),
    Dot(tokens::Dot),
    Colon(tokens::Colon),
    Semicolon(tokens::Semicolon),
    DotDot(tokens::DotDot),
    Plus(tokens::Plus),
    Minus(tokens::Minus),
    Star(tokens::Star),
    Slash(tokens::Slash),
    Percent(tokens::Percent),
    PlusEqual(tokens::PlusEqual),
    MinusEqual(tokens::MinusEqual),
    StarEqual(tokens::StarEqual),
    SlashEqual(tokens::SlashEqual),
    PercentEqual(tokens::PercentEqual),
    EqualEqual(tokens::EqualEqual),
    BangEqual(tokens::BangEqual),
    Greater(tokens::Greater),
    GreaterEqual(tokens::GreaterEqual),
    Less(tokens::Less),
    LessEqual(tokens::LessEqual),
    Bang(tokens::Bang),
    Tilde(tokens::Tilde),
    QuestionMark(tokens::QuestionMark),
    False(tokens::False),
    True(tokens::True),
    Nil(tokens::Nil),
    If(tokens::If),
    Else(tokens::Else),
    Check(tokens::Check),
    As(tokens::As),
    And(tokens::And),
    Or(tokens::Or),
    Not(tokens::Not),
    Function(tokens::Function),
    Return(tokens::Return),
    Break(tokens::Break),
    Continue(tokens::Continue),
    Ev(tokens::Ev),
    For(tokens::For),
    While(tokens::While),
    In(tokens::In),
    Let(tokens::Let),
    Const(tokens::Const),
    Equal(tokens::Equal),
    Exit(tokens::Exit),
    EOF(tokens::EOF),
}

impl TokenEnum {
    /// Get the name of the token for debugging purposes.
    pub fn name(&self) -> String {
        use TokenEnum::*;

        // Rusts reflection capabilities are very limited, hence the enormous boilerplate here.
        match self {
            Number(_) => "Number",
            Str(_) => "Str",
            Identifier(_) => "Identifier",
            LeftParen(_) => "LeftParen",
            RightParen(_) => "RightParen",
            LeftBrace(_) => "LeftBrace",
            RightBrace(_) => "RightBrace",
            LeftBracket(_) => "LeftBracket",
            RightBracket(_) => "RightBracket",
            Pipe(_) => "Pipe",
            Comma(_) => "Comma",
            Dot(_) => "Dot",
            Colon(_) => "Colon",
            Semicolon(_) => "Semicolon",
            DotDot(_) => "DotDot",
            Plus(_) => "Plus",
            Minus(_) => "Minus",
            Star(_) => "Star",
            Slash(_) => "Slash",
            Percent(_) => "Percent",
            PlusEqual(_) => "PlusEqual",
            MinusEqual(_) => "MinusEqual",
            StarEqual(_) => "StarEqual",
            SlashEqual(_) => "SlashEqual",
            PercentEqual(_) => "PercentEqual",
            EqualEqual(_) => "EqualEqual",
            BangEqual(_) => "BangEqual",
            Greater(_) => "Greater",
            GreaterEqual(_) => "GreaterEqual",
            Less(_) => "Less",
            LessEqual(_) => "LessEqual",
            Bang(_) => "Bang",
            Tilde(_) => "Tilde",
            QuestionMark(_) => "QuestionMark",
            False(_) => "False",
            True(_) => "True",
            Nil(_) => "Nil",
            If(_) => "If",
            Else(_) => "Else",
            Check(_) => "Check",
            As(_) => "As",
            And(_) => "And",
            Or(_) => "Or",
            Not(_) => "Not",
            Function(_) => "Function",
            Return(_) => "Return",
            Break(_) => "Break",
            Continue(_) => "Continue",
            Ev(_) => "Ev",
            For(_) => "For",
            While(_) => "While",
            In(_) => "In",
            Let(_) => "Let",
            Const(_) => "Const",
            Equal(_) => "Equal",
            Exit(_) => "Exit",
            EOF(_) => "EOF",
        }
        .to_string()
    }
}

impl std::fmt::Display for TokenEnum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use TokenEnum::*;

        match self {
            Number(number) => write!(f, "{}", number.value),
            Str(string) => write!(f, "\"{}\"", string.value),
            Identifier(identifier) => write!(f, "{}", identifier.value),
            LeftParen(_) => write!(f, "("),
            RightParen(_) => write!(f, ")"),
            LeftBrace(_) => write!(f, "{{"), // double braces to escape formatting argument
            RightBrace(_) => write!(f, "}}"),
            LeftBracket(_) => write!(f, "["),
            RightBracket(_) => write!(f, "]"),
            Pipe(_) => write!(f, "|"),
            Comma(_) => write!(f, ","),
            Dot(_) => write!(f, "."),
            Colon(_) => write!(f, ":"),
            Semicolon(_) => write!(f, ";"),
            DotDot(_) => write!(f, ".."),
            Plus(_) => write!(f, "+"),
            Minus(_) => write!(f, "-"),
            Star(_) => write!(f, "*"),
            Slash(_) => write!(f, "/"),
            Percent(_) => write!(f, "%"),
            PlusEqual(_) => write!(f, "+="),
            MinusEqual(_) => write!(f, "-="),
            StarEqual(_) => write!(f, "*="),
            SlashEqual(_) => write!(f, "/="),
            PercentEqual(_) => write!(f, "%="),
            EqualEqual(_) => write!(f, "=="),
            BangEqual(_) => write!(f, "!="),
            Greater(_) => write!(f, ">"),
            GreaterEqual(_) => write!(f, ">="),
            Less(_) => write!(f, "<"),
            LessEqual(_) => write!(f, "<="),
            Bang(_) => write!(f, "!"),
            Tilde(_) => write!(f, "~"),
            QuestionMark(_) => write!(f, "?"),
            False(_) => write!(f, "false"),
            True(_) => write!(f, "true"),
            Nil(_) => write!(f, "nil"),
            If(_) => write!(f, "if"),
            Else(_) => write!(f, "else"),
            Check(_) => write!(f, "check"),
            As(_) => write!(f, "as"),
            And(_) => write!(f, "and"),
            Or(_) => write!(f, "or"),
            Not(_) => write!(f, "not"),
            Function(_) => write!(f, "fn"),
            Return(_) => write!(f, "return"),
            Break(_) => write!(f, "Break"),
            Continue(_) => write!(f, "Continue"),
            Ev(_) => write!(f, "->"),
            For(_) => write!(f, "for"),
            While(_) => write!(f, "while"),
            In(_) => write!(f, "in"),
            Let(_) => write!(f, "let"),
            Const(_) => write!(f, "const"),
            Equal(_) => write!(f, "="),
            Exit(_) => write!(f, "exit"),
            EOF(_) => write!(f, "EOF"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn expect_test() {
        let token = Token {
            value: tokens::Number { value: 0.0 }.into(),
            span: Span::new((1, 1, 0), "test"),
        };

        assert!(token.expect::<tokens::False>().is_err());

        let number_token = token.expect::<tokens::Number>().unwrap();
        assert_eq!(number_token.value, 0.0);
    }
}
