use tracing::info;

use crate::prelude::*;
use crate::VALID_IDENTIFIER_CHARS;

pub const TARGET: &str = "vyder_core::lexer";

/// Convert raw source code to tokens.
pub struct Lexer {
    current_index: usize,

    current_line: usize,
    current_column: usize,

    source: String,
    module_name: String,
}

impl Lexer {
    /// Create a new lexer.
    pub fn new(module_name: impl Into<String>, source: &str) -> Self {
        Self {
            current_index: 0,

            current_line: 1,
            current_column: 1,

            source: source.trim().to_string(),
            module_name: module_name.into(),
        }
    }

    /// Convert the source code to tokens.
    pub fn get_tokens(&mut self) -> Result<Vec<Token>> {
        let mut tokens = vec![];

        if let Some('#') = self.peek(0) {
            loop {
                self.advance();
                if let Some('\n') = self.peek(0) {
                    break;
                }
            }
        }

        loop {
            let token = self.get_token()?;

            info!(target: TARGET, "Next token is '{}'", &token.value);

            if token.expect::<tokens::EOF>().is_ok() {
                break;
            }

            tokens.push(token);

            self.advance();
        }

        Ok(tokens)
    }

    /// Get the next token.
    fn get_token(&mut self) -> Result<Token> {
        info!(target: TARGET, "Normal get_token function");

        let mut current_span = self.get_current_span();

        let ch = match self.peek(0) {
            Some(ch) => ch,
            None => {
                return Ok(Token {
                    value: tokens::EOF.into(),
                    span: current_span,
                })
            }
        };

        let token: TokenEnum = match ch {
            '\n' => {
                self.current_line += 1;
                self.current_index += 1;
                self.current_column = 1;
                return self.get_token();
            }
            '\r' => {
                self.current_index += 1;
                self.current_column += 1;
                return self.get_token();
            }
            ' ' => {
                self.advance();
                return self.get_token();
            }
            '\t' => {
                self.current_column += 3;
                self.advance();
                return self.get_token();
            }
            '(' => tokens::LeftParen.into(),
            ')' => tokens::RightParen.into(),
            '{' => tokens::LeftBrace.into(),
            '}' => tokens::RightBrace.into(),
            '[' => tokens::LeftBracket.into(),
            ']' => tokens::RightBracket.into(),
            '|' => tokens::Pipe.into(),
            ',' => tokens::Comma.into(),
            '.' => {
                if let Some('.') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::DotDot.into()
                } else {
                    tokens::Dot.into()
                }
            }
            ':' => tokens::Colon.into(),
            ';' => tokens::Semicolon.into(),
            '+' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::PlusEqual.into()
                } else {
                    tokens::Plus.into()
                }
            }
            '-' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::MinusEqual.into()
                } else if let Some('>') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::Ev.into()
                } else {
                    tokens::Minus.into()
                }
            }
            '*' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::StarEqual.into()
                } else {
                    tokens::Star.into()
                }
            }
            '/' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::SlashEqual.into()
                } else if let Some('/') = self.peek(1) {
                    loop {
                        self.advance();
                        match self.peek(1) {
                            Some('\n') => {
                                self.current_line += 1;
                                self.advance();
                                self.advance();
                                self.current_column = 1;
                                return self.get_token();
                            }
                            Some(_) => {}
                            None => return self.get_token(),
                        }
                    }
                } else {
                    tokens::Slash.into()
                }
            }
            '%' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::PercentEqual.into()
                } else {
                    tokens::Percent.into()
                }
            }
            '=' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::EqualEqual.into()
                } else {
                    tokens::Equal.into()
                }
            }
            '!' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::BangEqual.into()
                } else {
                    tokens::Bang.into()
                }
            }
            '~' => tokens::Tilde.into(),
            '>' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::GreaterEqual.into()
                } else {
                    tokens::Greater.into()
                }
            }
            '<' => {
                if let Some('=') = self.peek(1) {
                    self.advance();
                    let mut end_location = current_span.get_start();
                    end_location.column += 1;
                    end_location.char_index += 1;
                    current_span.set_end(end_location);
                    tokens::LessEqual.into()
                } else {
                    tokens::Less.into()
                }
            }
            '?' => tokens::QuestionMark.into(),
            ch if ch.is_ascii_digit() => self.get_number(&mut current_span)?,
            ch if VALID_IDENTIFIER_CHARS.contains(ch) => self.get_identifier(&mut current_span)?,
            ch if ch == '"' || ch == '\'' => self.get_string(&mut current_span, ch)?,
            ch => {
                return Err(Error::new(
                    ErrorType::UnexpectedChar {
                        unexpected_char: ch,
                    },
                    current_span,
                ))
            }
        };

        Ok(Token {
            value: token,
            span: current_span,
        })
    }

    fn get_number(&mut self, current_span: &mut Span) -> Result<TokenEnum> {
        info!(target: TARGET, "get_number function");

        let mut raw_number = String::new();

        let mut had_decimal_point = false;

        let mut end_location = current_span.get_start();

        while let Some(ch) = self.peek(0) {
            if ch.is_ascii_digit() {
                raw_number.push(ch);
            } else if ch == '.' {
                if had_decimal_point {
                    break;
                }

                match self.peek(1) {
                    Some(ch) if ch.is_ascii_digit() => {}
                    _ => break,
                };

                had_decimal_point = true;
                raw_number.push('.');
            } else if ch != '_' {
                break;
            }

            self.advance();
            end_location.column += 1;
            end_location.char_index += 1;
        }

        self.current_column -= 1;
        self.current_index -= 1;
        end_location.char_index -= 1;
        end_location.column -= 1;

        current_span.set_end(end_location);

        Ok(tokens::Number {
            value: raw_number.parse().unwrap(),
        }
        .into())
    }

    fn get_string(&mut self, current_span: &mut Span, string_char: char) -> Result<TokenEnum> {
        info!(target: TARGET, "get_string function");

        self.advance();

        let mut end_location = current_span.get_start();

        let mut string = String::new();

        loop {
            let ch = match self.peek(0) {
                Some(ch) => ch,
                None => {
                    let line = end_location.line - 1;
                    return Err(Error::new(ErrorType::UnfinishedString, {
                        end_location.column += 1;
                        Span::new(end_location, self.module_name.clone())
                            .add_snippet(self.source.lines().nth(line).unwrap())
                    }));
                }
            };

            match ch {
                ch if ch == string_char => break, // reached end of string
                '\\' => {
                    let ch = match self.peek(1) {
                        Some(ch) => ch,
                        None => {
                            let line = end_location.line - 1;
                            return Err(Error::new(ErrorType::UnfinishedString, {
                                end_location.column += 2;
                                Span::new(end_location, self.module_name.clone())
                                    .add_snippet(self.source.lines().nth(line).unwrap())
                            }));
                        }
                    };

                    match ch {
                        't' => string.push('\t'),
                        'n' => string.push('\n'),
                        'r' => string.push('\r'),
                        '\\' => string.push('\\'),
                        '0' => string.push('\0'),
                        '"' => string.push('"'),
                        '\'' => string.push('\''),
                        _ => {
                            let line = end_location.line - 1;
                            return Err(Error::new(ErrorType::InvalidEscapeSequence, {
                                end_location.column += 2;
                                Span::new(end_location, self.module_name.clone())
                                    .add_snippet(self.source.lines().nth(line).unwrap())
                            }));
                        }
                    };

                    self.current_index += 1;
                    info!(target: TARGET, "Advancing to char index {} without increasing column", self.current_index);
                }
                ch => string.push(ch), // normal char
            };

            self.advance();
            end_location.column += 1;
            end_location.char_index += 1;
        }

        end_location.column += 1;
        end_location.char_index += 1;
        current_span.set_end(end_location);

        Ok(tokens::Str { value: string }.into())
    }

    fn get_identifier(&mut self, current_span: &mut Span) -> Result<TokenEnum> {
        let mut identifier = String::new();

        current_span.set_end(current_span.get_start());

        while let Some(ch) = self.peek(0) {
            if !VALID_IDENTIFIER_CHARS.contains(ch) {
                break;
            }

            identifier.push(ch);

            self.advance();

            let mut end_location = current_span.get_end();
            end_location.column += 1;
            end_location.char_index += 1;
            current_span.set_end(end_location);
        }

        let mut end_location = current_span.get_end();
        end_location.column -= 1;
        end_location.char_index -= 1;
        current_span.set_end(end_location);
        self.current_index -= 1;
        self.current_column -= 1;

        Ok(match identifier.as_str() {
            "false" => tokens::False.into(),
            "true" => tokens::True.into(),
            "nil" => tokens::Nil.into(),
            "if" => tokens::If.into(),
            "else" => tokens::Else.into(),
            "check" => tokens::Check.into(),
            "as" => tokens::As.into(),
            "and" => tokens::And.into(),
            "or" => tokens::Or.into(),
            "not" => tokens::Not.into(),
            "fn" => tokens::Function.into(),
            "return" => tokens::Return.into(),
            "break" => tokens::Break.into(),
            "continue" => tokens::Continue.into(),
            "for" => tokens::For.into(),
            "while" => tokens::While.into(),
            "in" => tokens::In.into(),
            "let" => tokens::Let.into(),
            "const" => tokens::Const.into(),
            "exit" => tokens::Exit.into(),
            _ => tokens::Identifier { value: identifier }.into(),
        })
    }

    fn get_current_span(&self) -> Span {
        info!(target: TARGET, "Constructing span at line {}, column {}", self.current_line, self.current_column);

        let span = Span::new(
            (self.current_line, self.current_column, self.current_index),
            self.module_name.clone(),
        );

        let line = span.get_start().line - 1;
        info!("getting line {}", line);
        let span = span.add_snippet(self.source.lines().nth(line).unwrap());

        span
    }

    fn peek(&self, distance: isize) -> Option<char> {
        let index = self.current_index as isize + distance;

        if index < 0 {
            return None;
        }

        let ch = self.source.chars().nth(index as usize);

        let ch_string = match ch {
            Some(ch) => ch.to_string(),
            None => "None".to_string(),
        };

        info!(target: TARGET, "Char at distance {} from {} is '{}'", distance, self.current_index, ch_string);

        ch
    }

    fn advance(&mut self) {
        self.current_index += 1;
        self.current_column += 1;
        info!(target: TARGET, "Advancing to char index {}", self.current_index);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tracing_test::traced_test;

    fn get_tracer() -> tracing_subscriber::FmtSubscriber<
        tracing_subscriber::fmt::format::DefaultFields,
        tracing_subscriber::fmt::format::Format,
        tracing_subscriber::EnvFilter,
    > {
        tracing_subscriber::fmt::Subscriber::builder()
            .with_env_filter(TARGET)
            .finish()
    }

    #[test]
    #[traced_test]
    fn comments_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let source = r#"#!vyder
+ // test 2
// test 3"#;
        let expected = [Token {
            value: tokens::Plus.into(),
            span: Span::new((2, 1, 8), "test"),
        }];

        test_lexer(source, &expected);
    }

    #[test]
    #[traced_test]
    fn operators_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let source = r#"+=
-- /"#;
        let expected = [
            Token {
                value: tokens::PlusEqual.into(),
                span: Span::new_with_end((1, 1, 0), (1, 2, 1), "test"),
            },
            Token {
                value: tokens::Minus.into(),
                span: Span::new((2, 1, 3), "test"),
            },
            Token {
                value: tokens::Minus.into(),
                span: Span::new((2, 2, 4), "test"),
            },
            Token {
                value: tokens::Slash.into(),
                span: Span::new((2, 4, 6), "test"),
            },
        ];

        test_lexer(source, &expected);
    }

    #[test]
    #[traced_test]
    fn identifiers_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let source = r#"foo return)"#;
        let expected = [
            Token {
                value: tokens::Identifier {
                    value: "foo".to_string(),
                }
                .into(),
                span: Span::new_with_end((1, 1, 0), (1, 3, 2), "test"),
            },
            Token {
                value: tokens::Return.into(),
                span: Span::new_with_end((1, 5, 4), (1, 10, 9), "test"),
            },
            Token {
                value: tokens::RightParen.into(),
                span: Span::new((1, 11, 10), "test"),
            },
        ];

        test_lexer(source, &expected);
    }

    #[test]
    #[traced_test]
    fn strings_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let source = r#"+"Hello, World!"'foo\t'("#;
        let expected = [
            Token {
                value: tokens::Plus.into(),
                span: Span::new((1, 1, 0), "test"),
            },
            Token {
                value: tokens::Str {
                    value: "Hello, World!".to_string(),
                }
                .into(),
                span: Span::new_with_end((1, 2, 1), (1, 16, 15), "test"),
            },
            Token {
                value: tokens::Str {
                    value: "foo\t".to_string(),
                }
                .into(),
                span: Span::new_with_end((1, 17, 16), (1, 22, 21), "test"),
            },
            Token {
                value: tokens::LeftParen.into(),
                span: Span::new((1, 23, 23), "test"), // char_index is 23 too because of the escape sequence
            },
        ];

        test_lexer(source, &expected);
    }

    #[test]
    #[traced_test]
    fn numbers_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let source = r#"12.5.foo 12"#;
        let expected = [
            Token {
                value: tokens::Number { value: 12.5 }.into(),
                span: Span::new_with_end((1, 1, 0), (1, 4, 3), "test"),
            },
            Token {
                value: tokens::Dot.into(),
                span: Span::new((1, 5, 4), "test"),
            },
            Token {
                value: tokens::Identifier {
                    value: "foo".to_string(),
                }
                .into(),
                span: Span::new_with_end((1, 6, 5), (1, 8, 7), "test"),
            },
            Token {
                value: tokens::Number { value: 12.0 }.into(),
                // span: Span::new(1, 10, 1),
                span: Span::new_with_end((1, 10, 9), (1, 11, 10), "test"),
            },
        ];

        test_lexer(source, &expected);
    }

    fn test_lexer(source: &str, expected: &[Token]) {
        let actual = Lexer::new("test", source).get_tokens().unwrap();

        for (expected, actual) in std::iter::zip(expected, actual) {
            assert_eq!(*expected, actual);
        }
    }
}
