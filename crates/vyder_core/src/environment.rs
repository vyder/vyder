use std::collections::HashMap;

use crate::prelude::*;

pub struct Environment {
    scopes: Vec<HashMap<String, (ValueResult, bool)>>,
}

impl Environment {
    pub fn new() -> Self {
        Self {
            scopes: vec![HashMap::new()],
        }
    }

    pub fn define(&mut self, identifier: &str, value: ValueResult, is_const: bool) {
        self.scopes
            .last_mut()
            .expect("should never be able to remove the root scope")
            .insert(identifier.to_string(), (value, is_const));
    }

    pub fn assign(&mut self, identifier: &str, value: ValueResult, span: &Span) -> Result<()> {
        let mut scope_index = self.scopes.len() - 1;

        loop {
            let scope = self
                .scopes
                .get_mut(scope_index)
                .expect("scope_index should never be invalid");

            match scope.get(identifier) {
                Some((_, false)) => {
                    scope.insert(identifier.to_string(), (value, false));
                    return Ok(());
                }
                Some((_, true)) => {
                    return Err(Error::new(
                        ErrorType::CannotAssignToConstant(identifier.to_string()),
                        span.clone(),
                    ))
                }
                None => {}
            };

            if scope_index == 0 {
                return Err(Error::new(
                    ErrorType::UndefinedVariable(identifier.to_string()),
                    span.clone(),
                ));
            }

            scope_index -= 1;
        }
    }

    pub fn get(&self, identifier: &str, span: &Span) -> Result<&ValueResult> {
        let mut scope_index = self.scopes.len() - 1;

        loop {
            let scope = self.scopes.get(scope_index).unwrap();

            if let Some((value, _)) = scope.get(identifier) {
                return Ok(value);
            }

            if scope_index == 0 {
                return Err(Error::new(
                    ErrorType::UndefinedVariable(identifier.to_string()),
                    span.clone(),
                ));
            }

            scope_index -= 1;
        }
    }

    pub fn enter_scope(&mut self) {
        self.scopes.push(HashMap::new());
    }

    pub fn exit_scope(&mut self) {
        assert!(self.scopes.len() > 1, "tried to remove the root scope");

        self.scopes.pop();
    }
}

impl Default for Environment {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn environments_test() {
        let mut environment = Environment::default();

        environment.define(
            "foo",
            ValueResult::Ok(Value::new_value(
                values::Bool { value: true }.into(),
                Span::new((1, 1, 0), "test"),
            )),
            false,
        );

        assert!(environment
            .get("bar", &Span::new((1, 1, 0), "test"))
            .is_err());

        environment.enter_scope();

        environment.define(
            "foo",
            ValueResult::Ok(Value::new_value(
                values::Bool { value: false }.into(),
                Span::new((1, 1, 0), "test"),
            )),
            false,
        );

        assert!(
            !environment
                .get("foo", &Span::new((1, 1, 0), "test"))
                .unwrap()
                .ignore_error()
                .expect::<values::Bool>()
                .unwrap()
                .value
        );

        environment.exit_scope();

        assert!(
            environment
                .get("foo", &Span::new((1, 1, 0), "test"))
                .unwrap()
                .ignore_error()
                .expect::<values::Bool>()
                .unwrap()
                .value
        );
    }
}
