use std::process::ExitCode;

mod cli;

fn print(
    arguments: &[vyder_core::ValueResult],
    span: vyder_core::Span,
) -> Result<(vyder_core::ValueResult, Option<ExitCode>), vyder_core::Error> {
    let mut out = String::new();

    for argument in arguments {
        argument.clone().expect_non_error()?;

        out.push_str(&argument.to_string());
    }

    println!("{}", out);

    Ok((
        vyder_core::Value::new_ok_value(vyder_core::values::Nil.into(), span),
        None,
    ))
}

fn main() {
    tracing_subscriber::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .init();

    let matches = cli::build_cli().get_matches();

    match matches.subcommand() {
        Some(("tokens", matches)) => {
            let source = match matches.get_one::<String>("source") {
                Some(source) => source.clone(),
                None => {
                    if matches.get_flag("stdin") {
                        let mut buffer = String::new();

                        for line in std::io::stdin().lines() {
                            buffer.push_str(&format!("{}\n", line.unwrap()));
                        }

                        buffer
                    } else {
                        eprintln!("no source given.");
                        return;
                    }
                }
            };

            let tokens = match vyder_core::Lexer::new("debug_cli", &source).get_tokens() {
                Ok(tokens) => tokens,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            for token in tokens {
                println!("{}", token);
            }
        }
        Some(("expression", matches)) => {
            let source = match matches.get_one::<String>("source") {
                Some(source) => source.clone(),
                None => {
                    if matches.get_flag("stdin") {
                        let mut buffer = String::new();

                        for line in std::io::stdin().lines() {
                            buffer.push_str(&format!("{}\n", line.unwrap()));
                        }

                        buffer
                    } else {
                        eprintln!("no source given.");
                        return;
                    }
                }
            };

            let tokens = match vyder_core::Lexer::new("debug_cli", &source).get_tokens() {
                Ok(tokens) => tokens,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            let mut parser = vyder_core::Parser::new("debug_cli", tokens.as_slice());

            let expression = match parser.get_expression() {
                Ok(Some(expression)) => expression,
                Ok(None) => {
                    eprintln!("no expression");
                    return;
                }
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            println!(
                "{}",
                vyder_core::display_tree::display_expression(&expression)
            );
        }
        Some(("statements", matches)) => {
            let source = match matches.get_one::<String>("source") {
                Some(source) => source.clone(),
                None => {
                    if matches.get_flag("stdin") {
                        let mut buffer = String::new();

                        for line in std::io::stdin().lines() {
                            buffer.push_str(&format!("{}\n", line.unwrap()));
                        }

                        buffer
                    } else {
                        eprintln!("no source given.");
                        return;
                    }
                }
            };

            let tokens = match vyder_core::Lexer::new("debug_cli", &source).get_tokens() {
                Ok(tokens) => tokens,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            let mut parser = vyder_core::Parser::new("debug_cli", tokens.as_slice());

            let statements = match parser.get_statements() {
                Ok(statements) => statements,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            for statement in statements {
                println!(
                    "{}",
                    vyder_core::display_tree::display_statement(&statement)
                );
            }
        }
        Some(("evaluate", matches)) => {
            let source = match matches.get_one::<String>("source") {
                Some(source) => source.clone(),
                None => {
                    if matches.get_flag("stdin") {
                        let mut buffer = String::new();

                        for line in std::io::stdin().lines() {
                            buffer.push_str(&format!("{}\n", line.unwrap()));
                        }

                        buffer
                    } else {
                        eprintln!("no source given.");
                        return;
                    }
                }
            };

            let tokens = match vyder_core::Lexer::new("debug_cli", &source).get_tokens() {
                Ok(tokens) => tokens,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            let mut parser = vyder_core::Parser::new("debug_cli", tokens.as_slice());

            let expression = match parser.get_expression() {
                Ok(Some(expression)) => expression,
                Ok(None) => {
                    eprintln!("no expression");
                    return;
                }
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            let mut interpreter = vyder_core::Interpreter::new("debug_cli", ".", vec![]);
            let value = match interpreter.evaluate_expression(expression) {
                Ok(value) => value,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            }
            .get_value();

            if let Some(value) = value {
                println!("{}", value);
            }
        }
        Some(("interpret", matches)) => {
            let source = match matches.get_one::<String>("source") {
                Some(source) => source.clone(),
                None => {
                    if matches.get_flag("stdin") {
                        let mut buffer = String::new();

                        for line in std::io::stdin().lines() {
                            buffer.push_str(&format!("{}\n", line.unwrap()));
                        }

                        buffer
                    } else {
                        eprintln!("no source given.");
                        return;
                    }
                }
            };

            let tokens = match vyder_core::Lexer::new("debug_cli", &source).get_tokens() {
                Ok(tokens) => tokens,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            let mut parser = vyder_core::Parser::new("debug_cli", tokens.as_slice());

            let statements = match parser.get_statements() {
                Ok(statements) => statements,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            let mut interpreter = vyder_core::Interpreter::new("debug_cli", ".", vec![]);
            interpreter.environment.define(
                "print",
                vyder_core::Value::new_ok_value(
                    vyder_core::values::Callable::Lib {
                        function: print,
                        can_be_error: false,
                    }
                    .into(),
                    vyder_core::Span::new((1, 1, 0), "debug_cli"),
                ),
                true,
            );

            let result = match interpreter.interpret_statements(statements) {
                Ok(result) => result,
                Err(e) => {
                    eprintln!("{}", e);
                    return;
                }
            };

            match result {
                vyder_core::ControlFlow::Return(value) => println!("{}", value),
                vyder_core::ControlFlow::Ev(value) => println!("{}", value),
                _ => {}
            };
        }
        _ => unreachable!(),
    };
}
