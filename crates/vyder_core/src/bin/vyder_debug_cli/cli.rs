use clap::{crate_authors, crate_version, Arg, ArgAction, Command};

pub fn build_cli() -> Command {
    Command::new("vyder_debug_cli")
        .author(crate_authors!())
        .version(crate_version!())
        .about("tools for debugging vyder_core")
        .subcommand_required(true)
        .subcommand(
            Command::new("tokens")
                .about("print tokens generated from a string.")
                .arg(
                    Arg::new("stdin")
                        .long("stdin")
                        .action(ArgAction::SetTrue)
                        .help("take the source code from stdin."),
                )
                .arg(Arg::new("source").help("the source code to lex.")),
        )
        .subcommand(
            Command::new("expression")
                .about("print expression generated from a string.")
                .arg(
                    Arg::new("stdin")
                        .long("stdin")
                        .action(ArgAction::SetTrue)
                        .help("take the source code from stdin."),
                )
                .arg(Arg::new("source").help("the source code to parse.")),
        )
        .subcommand(
            Command::new("statements")
                .about("print statements generated from a string.")
                .arg(
                    Arg::new("stdin")
                        .long("stdin")
                        .action(ArgAction::SetTrue)
                        .help("take the source code from stdin."),
                )
                .arg(Arg::new("source").help("the source code to parse.")),
        )
        .subcommand(
            Command::new("evaluate")
                .about("evaluate an expression.")
                .arg(
                    Arg::new("stdin")
                        .long("stdin")
                        .action(ArgAction::SetTrue)
                        .help("take the source code from stdin."),
                )
                .arg(Arg::new("source").help("the source code to parse.")),
        )
        .subcommand(
            Command::new("interpret")
                .about("interpret statements.")
                .arg(
                    Arg::new("stdin")
                        .long("stdin")
                        .action(ArgAction::SetTrue)
                        .help("take the source code from stdin."),
                )
                .arg(Arg::new("source").help("the source code to parse.")),
        )
}
