use std::process::ExitCode;

use crate::prelude::*;

#[derive(Clone, Debug, IntoValueEnum, PartialEq)]
pub struct Number {
    pub value: f64,
}

#[derive(Clone, Debug, IntoValueEnum, PartialEq)]
pub struct Bool {
    pub value: bool,
}

#[derive(Clone, Debug, IntoValueEnum, PartialEq)]
pub struct Nil;

#[derive(Clone, Debug, IntoValueEnum, PartialEq)]
pub struct Str {
    pub value: String,
}

#[derive(Clone, Debug, IntoValueEnum, PartialEq)]
pub enum Range {
    Closed { rhs: i64, lhs: i64 },
    From { lhs: i64 },
    To { rhs: i64 },
}

#[derive(Clone, Debug, IntoValueEnum, PartialEq)]
pub struct Map {
    pub values: Vec<(ValueEnum, ValueEnum)>,
}

#[derive(Clone, Debug, IntoValueEnum, PartialEq)]
pub enum Callable {
    User {
        parameters: Vec<crate::Identifier>,
        body: Box<Expression>,
    },
    Lib {
        #[allow(clippy::type_complexity)]
        function: fn(&[ValueResult], Span) -> Result<(ValueResult, Option<ExitCode>)>,
        can_be_error: bool,
    },
}
