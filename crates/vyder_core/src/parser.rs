use tracing::info;

use crate::prelude::*;

const TARGET: &str = "vyder_core::parser";

#[allow(unused)] // for the module_name field
pub struct Parser {
    current_index: usize,
    tokens: Vec<Token>,
    last_span: Span,
    module_name: String,
}

impl Parser {
    pub fn new(module_name: impl Into<String>, tokens: &[Token]) -> Self {
        let module_name: String = module_name.into();

        let span = Span::new((1, 1, 0), module_name.clone());
        let last_token_span = match tokens.last() {
            Some(last_token) => &last_token.span,
            None => &span,
        };

        let mut last_location = last_token_span.get_end();
        last_location.column += 1;
        let last_span = Span::new(last_location, module_name.clone());

        Self {
            current_index: 0,
            tokens: tokens.into(),
            last_span,
            module_name,
        }
    }

    pub fn get_statements(&mut self) -> Result<Vec<Statement>> {
        let mut statements = vec![];

        loop {
            let statement = match self.get_statement()? {
                Some(statement) => statement,
                None => break,
            };

            info!(target: TARGET, "parsed statement '{}'", statement);

            statements.push(statement);
        }

        Ok(statements)
    }

    pub fn get_statement(&mut self) -> Result<Option<Statement>> {
        info!(target: TARGET, "get_statement function");

        let token = self.peek(0);

        if token.is_none() {
            return Ok(None);
        };

        let token = token.unwrap();

        let start_span = token.span.clone();

        let statement = match &token.value {
            TokenEnum::Return(_) => {
                self.advance();

                let expression = self.get_expression()?;

                statements::Return {
                    expression: expression.map(Box::new),
                }
                .into()
            }
            TokenEnum::Break(_) => {
                self.advance();
                statements::Break.into()
            }
            TokenEnum::Continue(_) => {
                self.advance();
                statements::Continue.into()
            }
            TokenEnum::Exit(_) => {
                self.advance();

                let expression = self.get_expression()?;

                statements::Exit {
                    expression: expression.map(Box::new),
                }
                .into()
            }
            TokenEnum::Ev(_) => {
                self.advance();

                let expression = self.get_expression()?;

                statements::Ev {
                    expression: expression.map(Box::new),
                }
                .into()
            }
            TokenEnum::Let(_) | TokenEnum::Const(_) => {
                let is_const = self.peek(0).unwrap().expect::<tokens::Const>().is_ok();

                self.advance();

                let identifier = self
                    .peek(0)
                    .expect_token(&["Identifier"], &self.get_current_span())?;
                let identifier_span = identifier.span.clone();
                let identifier = identifier.expect::<tokens::Identifier>()?;

                self.advance();

                let can_be_error = match self
                    .peek(0)
                    .expect_token(&["QuestionMark", "Equal"], &self.last_span)?
                    .expect::<tokens::QuestionMark>()
                {
                    Ok(_) => {
                        self.advance();
                        true
                    }
                    Err(_) => false,
                };

                let identifier =
                    crate::Identifier::new(&identifier.value, identifier_span, can_be_error)?;

                self.peek(0)
                    .expect_token(&["Equal"], &self.get_current_span())?
                    .expect::<tokens::Equal>()?;
                self.advance();

                let expression = self
                    .get_expression()?
                    .expect_expression(&self.get_current_span())?;

                statements::Declaration {
                    is_const,
                    identifier,
                    expression: Box::new(expression),
                }
                .into()
            }
            _ => {
                let expression = match self.get_expression()? {
                    Some(expression) => expression,
                    None => {
                        return Ok(None);
                    }
                };

                statements::Expression {
                    expression: Box::new(expression),
                }
                .into()
            }
        };

        let end_token = self
            .peek(0)
            .expect_token(&["Semicolon"], &self.get_current_span())?;

        info!(target: TARGET, "token after statement parse is '{}'", end_token);

        end_token.expect::<tokens::Semicolon>()?;
        self.advance();

        let end_span = end_token.span;

        info!(target: TARGET, "exit get_statement");

        Ok(Some(Statement {
            value: statement,
            span: Span::combine(&start_span, &end_span),
        }))
    }

    pub fn get_expression(&mut self) -> Result<Option<Expression>> {
        let expression = self.get_assignment();

        if let Ok(Some(expression)) = &expression {
            info!(target: TARGET, "Parsed expression '{}'", expression);
        };

        expression
    }

    fn get_assignment(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_expression function");

        let expression = match self.get_combiner()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        let token = match self.peek(0) {
            Some(token) => token,
            None => return Ok(Some(expression)),
        };

        let constructor = match &token.value {
            TokenEnum::Equal(_) => expressions::Assignment::new_as_expression,
            TokenEnum::PlusEqual(_) => expressions::AdditionAssignment::new_as_expression,
            TokenEnum::MinusEqual(_) => expressions::SubtractionAssignment::new_as_expression,
            TokenEnum::StarEqual(_) => expressions::MultiplicationAssignment::new_as_expression,
            TokenEnum::SlashEqual(_) => expressions::DivisionAssignment::new_as_expression,
            TokenEnum::PercentEqual(_) => expressions::ModuloAssignment::new_as_expression,
            _ => return Ok(Some(expression)),
        };

        let identifier = expression.expect::<expressions::Identifier>()?;

        self.advance();

        let value = match self.get_assignment()? {
            Some(value) => value,
            None => return Ok(None),
        };

        let span = Span::combine(&expression.span, &value.span);

        Ok(Some(constructor(
            crate::Identifier::new(&identifier.value, expression.span, false).unwrap(),
            value,
            span,
        )))
    }

    fn get_combiner(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_combiner function");

        let expression = match self.get_equality()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        let token = match self.peek(0) {
            Some(token) => token,
            None => return Ok(Some(expression)),
        };

        let constructor = match &token.value {
            TokenEnum::Or(_) => expressions::Or::new_as_expression,
            TokenEnum::And(_) => expressions::And::new_as_expression,
            _ => return Ok(Some(expression)),
        };

        let lhs = expression;

        self.advance();

        let rhs = match self.get_combiner()? {
            Some(rhs) => rhs,
            None => return Ok(None),
        };

        let span = Span::combine(&lhs.span, &rhs.span);

        Ok(Some(constructor(lhs, rhs, span)))
    }

    fn get_equality(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_equality function");

        let expression = match self.get_comparison()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        let token = match self.peek(0) {
            Some(token) => token,
            None => return Ok(Some(expression)),
        };

        let constructor = match &token.value {
            TokenEnum::EqualEqual(_) => expressions::Equal::new_as_expression,
            TokenEnum::BangEqual(_) => expressions::NotEqual::new_as_expression,
            _ => return Ok(Some(expression)),
        };

        let lhs = expression;

        self.advance();

        let rhs = match self.get_equality()? {
            Some(rhs) => rhs,
            None => return Ok(None),
        };

        let span = Span::combine(&lhs.span, &rhs.span);

        Ok(Some(constructor(lhs, rhs, span)))
    }

    fn get_comparison(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_comparison function");

        let expression = match self.get_range()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        let token = match self.peek(0) {
            Some(token) => token,
            None => return Ok(Some(expression)),
        };

        let constructor = match &token.value {
            TokenEnum::Greater(_) => expressions::Greater::new_as_expression,
            TokenEnum::GreaterEqual(_) => expressions::GreaterEqual::new_as_expression,
            TokenEnum::Less(_) => expressions::Less::new_as_expression,
            TokenEnum::LessEqual(_) => expressions::LessEqual::new_as_expression,
            _ => return Ok(Some(expression)),
        };

        let lhs = expression;

        self.advance();

        let rhs = match self.get_comparison()? {
            Some(rhs) => rhs,
            None => return Ok(None),
        };

        let span = Span::combine(&lhs.span, &rhs.span);

        Ok(Some(constructor(lhs, rhs, span)))
    }

    /// TODO: support open-ended and ranges without a start too
    fn get_range(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_range function");

        let expression = match self.get_term()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        let token = match self.peek(0) {
            Some(token) => token,
            None => return Ok(Some(expression)),
        };

        if token.expect::<tokens::DotDot>().is_err() {
            return Ok(Some(expression));
        }

        let lhs = expression;

        self.advance();

        let rhs = match self.get_term()? {
            Some(rhs) => rhs,
            None => return Ok(None),
        };

        let span = Span::combine(&lhs.span, &rhs.span);

        Ok(Some(Expression {
            span,
            value: expressions::Range::Closed {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
            .into(),
        }))
    }

    fn get_term(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_term function");

        let expression = match self.get_factor()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        let token = match self.peek(0) {
            Some(token) => token,
            None => return Ok(Some(expression)),
        };

        let constructor = match &token.value {
            TokenEnum::Plus(_) => expressions::Addition::new_as_expression,
            TokenEnum::Minus(_) => expressions::Subtraction::new_as_expression,
            _ => return Ok(Some(expression)),
        };

        let lhs = expression;

        self.advance();

        let rhs = match self.get_term()? {
            Some(rhs) => rhs,
            None => return Ok(None),
        };

        let span = Span::combine(&lhs.span, &rhs.span);

        Ok(Some(constructor(lhs, rhs, span)))
    }

    fn get_factor(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_factor function");

        let expression = match self.get_unary()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        let token = match self.peek(0) {
            Some(token) => token,
            None => return Ok(Some(expression)),
        };

        let constructor = match &token.value {
            TokenEnum::Star(_) => expressions::Multiplication::new_as_expression,
            TokenEnum::Slash(_) => expressions::Division::new_as_expression,
            TokenEnum::Percent(_) => expressions::Modulo::new_as_expression,
            _ => return Ok(Some(expression)),
        };

        let lhs = expression;

        self.advance();

        let rhs = match self.get_factor()? {
            Some(rhs) => rhs,
            None => return Ok(None),
        };

        let span = Span::combine(&lhs.span, &rhs.span);

        Ok(Some(constructor(lhs, rhs, span)))
    }

    fn get_unary(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_unary function");

        let token = match self.peek(0) {
            Some(token) => token.clone(),
            None => return self.get_properties(),
        };

        if token.expect::<tokens::Minus>().is_err()
            && token.expect::<tokens::Bang>().is_err()
            && token.expect::<tokens::Tilde>().is_err()
            && token.expect::<tokens::QuestionMark>().is_err()
        {
            return self.get_properties();
        }

        self.advance();

        let expression = self.get_unary()?.expect_expression(&self.last_span)?;

        let constructor = match token.value {
            TokenEnum::Minus(_) => expressions::Negate::new_as_expression,
            TokenEnum::Bang(_) => expressions::Not::new_as_expression,
            TokenEnum::Tilde(_) => expressions::AsError::new_as_expression,
            TokenEnum::QuestionMark(_) => expressions::Expect::new_as_expression,
            _ => unreachable!(),
        };

        let span = Span::combine(&token.span, &expression.span);

        Ok(Some(constructor(expression.clone(), span)))
    }

    fn get_properties(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_properties function");

        let mut expression = match self.get_primary()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        loop {
            match self.peek(0) {
                Some(Token {
                    value: TokenEnum::LeftBracket(_),
                    span: _,
                }) => {
                    self.advance();

                    let index = self.get_expression()?.expect_expression(&self.last_span)?;

                    let end_span = match self.peek(0) {
                        Some(Token {
                            value: TokenEnum::RightBracket(_),
                            span,
                        }) => span.clone(),
                        Some(token) => {
                            return Err(Error::new(
                                ErrorType::UnexpectedToken {
                                    expected: vec!["RightBracket".to_string()],
                                    actual: token.value.name(),
                                },
                                self.last_span.clone(),
                            ))
                        }
                        None => {
                            return Err(Error::new(
                                ErrorType::UnexpectedToken {
                                    expected: vec!["RightBracket".to_string()],
                                    actual: "EOF".to_string(),
                                },
                                self.last_span.clone(),
                            ))
                        }
                    };
                    expression = Expression {
                        span: Span::combine(&expression.span, &end_span),
                        value: expressions::Index {
                            expression: Box::new(expression),
                            index: Box::new(index),
                        }
                        .into(),
                    };

                    self.advance();
                }
                Some(Token {
                    value: TokenEnum::LeftParen(_),
                    span: _,
                }) => {
                    self.advance();

                    let mut arguments = vec![];

                    let end_span = 'outer: loop {
                        match self.peek(0).expect_token(&[], &self.last_span)? {
                            Token {
                                value: TokenEnum::RightParen(_),
                                span,
                            } => break 'outer span.clone(),
                            _ => {
                                arguments.push(
                                    self.get_expression()?.expect_expression(&self.last_span)?,
                                );

                                match self.peek(0) {
                                    Some(token) if token.expect::<tokens::Comma>().is_ok() => {
                                        self.advance()
                                    }
                                    Some(token) if token.expect::<tokens::RightParen>().is_ok() => {
                                    }
                                    token => {
                                        return Err(Error::new(
                                            ErrorType::UnexpectedToken {
                                                expected: vec![
                                                    "Comma".to_string(),
                                                    "RightParen".to_string(),
                                                ],
                                                actual: token
                                                    .map(|v| v.value.name())
                                                    .unwrap_or("EOF".to_string()),
                                            },
                                            self.last_span.clone(),
                                        ))
                                    }
                                }
                            }
                        }
                    };

                    self.advance();

                    expression = Expression {
                        span: Span::combine(&expression.span, &end_span),
                        value: expressions::Call {
                            expression: Box::new(expression),
                            arguments,
                        }
                        .into(),
                    };
                }
                Some(Token {
                    value: TokenEnum::Dot(_),
                    span: _,
                }) => {
                    self.advance();

                    let (identifier, span) = match self.peek(0) {
                        Some(token) => {
                            let span = token.span.clone();
                            let identifier = token.expect::<tokens::Identifier>()?;
                            (identifier, span)
                        }
                        None => {
                            return Err(Error::new(
                                ErrorType::UnexpectedToken {
                                    expected: vec!["Identifier".to_string()],
                                    actual: "EOF".to_string(),
                                },
                                self.last_span.clone(),
                            ))
                        }
                    };

                    self.advance();

                    expression = Expression {
                        span: Span::combine(&expression.span, &span),
                        value: expressions::FieldAccess {
                            expression: Box::new(expression),
                            field: crate::Identifier::new(&identifier.value, span, false).unwrap(),
                        }
                        .into(),
                    };

                    info!(target: TARGET, "ending branch for field access at token {:?}", self.peek(0));
                }
                _ => return Ok(Some(expression)),
            }
        }
    }

    fn get_primary(&mut self) -> Result<Option<Expression>> {
        info!(target: TARGET, "get_primary function");

        let token = self.peek(0).expect_token(&[], &self.last_span)?;

        let start_span = token.span.clone();
        let mut end_span: Option<Span> = None;

        let expression_enum: ExpressionEnum = match &token.value {
            TokenEnum::Number(number) => {
                self.advance();
                expressions::Number {
                    value: number.value,
                }
                .into()
            }
            TokenEnum::False(_) => {
                self.advance();
                expressions::False.into()
            }
            TokenEnum::True(_) => {
                self.advance();
                expressions::True.into()
            }
            TokenEnum::Nil(_) => {
                self.advance();
                expressions::Nil.into()
            }
            TokenEnum::Identifier(identifier) => {
                self.advance();
                expressions::Identifier {
                    value: identifier.value.clone(),
                }
                .into()
            }
            TokenEnum::Str(string) => {
                self.advance();
                expressions::Str {
                    value: string.value.clone(),
                }
                .into()
            }
            TokenEnum::LeftParen(_) => {
                self.advance();
                let expression = self.get_expression()?.expect_expression(&self.last_span)?;

                end_span = match self.peek(0) {
                    Some(Token {
                        span: end_span,
                        value: TokenEnum::RightParen(_),
                    }) => Some(end_span.clone()),
                    token => {
                        return Err(Error::new(
                            ErrorType::UnexpectedToken {
                                expected: vec!["RightParen".to_string()],
                                actual: token.map(|v| v.value.name()).unwrap_or("EOF".to_string()),
                            },
                            token
                                .map(|v| v.span.clone())
                                .unwrap_or(self.last_span.clone()),
                        ))
                    }
                };

                self.advance();

                expressions::Group {
                    value: Box::new(expression),
                }
                .into()
            }
            TokenEnum::While(_) => {
                self.advance();

                let can_be_error = match self
                    .peek(0)
                    .expect_token(&["QuestionMark", "LeftParen"], &self.last_span)?
                    .expect::<tokens::QuestionMark>()
                {
                    Ok(_) => {
                        self.advance();
                        true
                    }
                    Err(_) => false,
                };

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::LeftParen>()?;
                self.advance();

                let condition = self.get_expression()?.expect_expression(&self.last_span)?;

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::RightParen>()?;
                self.advance();

                let body = self.get_expression()?.expect_expression(&self.last_span)?;

                end_span = Some(body.span.clone());

                expressions::While {
                    can_be_error,
                    condition: Box::new(condition),
                    body: Box::new(body),
                }
                .into()
            }
            TokenEnum::Function(_) => {
                self.advance();

                let can_be_error = match self
                    .peek(0)
                    .expect_token(&["QuestionMark", "LeftParen"], &self.last_span)?
                    .expect::<tokens::QuestionMark>()
                {
                    Ok(_) => {
                        self.advance();
                        true
                    }
                    Err(_) => false,
                };

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::LeftParen>()?;
                self.advance();

                let mut parameters = vec![];

                loop {
                    let parameter = self
                        .peek(0)
                        .expect_token(&["Identifier", "RightParen"], &self.last_span)?;

                    if parameter.expect::<tokens::RightParen>().is_ok() {
                        break;
                    }

                    let identifier_token = parameter.expect::<tokens::Identifier>()?;

                    self.advance();

                    let can_be_error = match self
                        .peek(0)
                        .expect_token(&["Comma", "RightParen", "QuestionMark"], &self.last_span)?
                        .expect::<tokens::QuestionMark>()
                    {
                        Ok(_) => {
                            self.advance();
                            true
                        }
                        Err(_) => false,
                    };

                    match self
                        .peek(0)
                        .expect_token(&["Comma", "RightParen"], &self.last_span)?
                    {
                        Token {
                            value: TokenEnum::Comma(_),
                            span: _,
                        } => self.advance(),
                        Token {
                            value: TokenEnum::RightParen(_),
                            span: _,
                        } => {}
                        token => {
                            return Err(Error::new(
                                ErrorType::UnexpectedToken {
                                    expected: vec!["Comma".to_string(), "RightParen".to_string()],
                                    actual: token.value.name(),
                                },
                                token.span.clone(),
                            ))
                        }
                    }

                    parameters.push(
                        crate::Identifier::new(
                            &identifier_token.value,
                            parameter.span.clone(),
                            can_be_error,
                        )
                        .unwrap(),
                    );
                }

                self.advance();

                let body = self.get_expression()?.expect_expression(&self.last_span)?;

                end_span = Some(body.span.clone());

                expressions::Function {
                    can_be_error,
                    parameters,
                    body: Box::new(body),
                }
                .into()
            }
            TokenEnum::For(_) => {
                self.advance();

                let can_be_error = match self
                    .peek(0)
                    .expect_token(&["QuestionMark", "LeftParen"], &self.last_span)?
                    .expect::<tokens::QuestionMark>()
                {
                    Ok(_) => {
                        self.advance();
                        true
                    }
                    Err(_) => false,
                };

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::LeftParen>()?;
                self.advance();

                let identifier_token = self
                    .peek(0)
                    .expect_token(&["Identifier"], &self.last_span)?;
                self.advance();

                let identifier_span = identifier_token.span.clone();
                let identifier = identifier_token.expect::<tokens::Identifier>()?;

                let identifier_can_be_error = match self
                    .peek(0)
                    .expect_token(&["QuestionMark", "LeftParen"], &self.last_span)?
                    .expect::<tokens::QuestionMark>()
                {
                    Ok(_) => {
                        self.advance();
                        true
                    }
                    Err(_) => false,
                };

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::In>()?;
                self.advance();

                let to_loop_over = self.get_expression()?.expect_expression(&self.last_span)?;

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::RightParen>()?;
                self.advance();

                let body = self.get_expression()?.expect_expression(&self.last_span)?;

                expressions::For {
                    can_be_error,
                    identifier: crate::Identifier::new(
                        &identifier.value,
                        identifier_span,
                        identifier_can_be_error,
                    )
                    .unwrap(),
                    to_loop_over: Box::new(to_loop_over),
                    body: Box::new(body),
                }
                .into()
            }
            TokenEnum::Check(_) => {
                self.advance();

                let can_be_error = match self
                    .peek(0)
                    .expect_token(&["QuestionMark", "LeftParen"], &self.last_span)?
                    .expect::<tokens::QuestionMark>()
                {
                    Ok(_) => {
                        self.advance();
                        true
                    }
                    Err(_) => false,
                };

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::LeftParen>()?;
                self.advance();

                let expression_to_check =
                    self.get_expression()?.expect_expression(&self.last_span)?;

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::RightParen>()?;
                self.advance();

                let alias = if self
                    .peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::As>()
                    .is_ok()
                {
                    self.advance();

                    let identifier_token = self
                        .peek(0)
                        .expect_token(&["Identifier"], &self.last_span)?;

                    let identifier = identifier_token.expect::<tokens::Identifier>()?;
                    self.advance();

                    let can_be_error = match self
                        .peek(0)
                        .expect_token(&[], &self.last_span)?
                        .expect::<tokens::QuestionMark>()
                    {
                        Ok(_) => {
                            self.advance();
                            true
                        }
                        Err(_) => false,
                    };

                    Some(
                        crate::Identifier::new(
                            &identifier.value,
                            identifier_token.span.clone(),
                            can_be_error,
                        )
                        .unwrap(),
                    )
                } else {
                    None
                };

                let body = self.get_expression()?.expect_expression(&self.last_span)?;

                let else_alias: Option<crate::Identifier>;

                let else_body = match self.peek(0) {
                    Some(Token {
                        value: TokenEnum::Else(_),
                        span: _,
                    }) => {
                        self.advance();

                        else_alias = if self
                            .peek(0)
                            .expect_token(&[], &self.last_span)?
                            .expect::<tokens::As>()
                            .is_ok()
                        {
                            self.advance();

                            let identifier_token = self
                                .peek(0)
                                .expect_token(&["Identifier"], &self.last_span)?;

                            let identifier = identifier_token.expect::<tokens::Identifier>()?;
                            self.advance();

                            let can_be_error = match self
                                .peek(0)
                                .expect_token(&[], &self.last_span)?
                                .expect::<tokens::QuestionMark>()
                            {
                                Ok(_) => {
                                    self.advance();
                                    true
                                }
                                Err(_) => false,
                            };

                            Some(
                                crate::Identifier::new(
                                    &identifier.value,
                                    identifier_token.span.clone(),
                                    can_be_error,
                                )
                                .unwrap(),
                            )
                        } else {
                            None
                        };

                        Some(Box::new(
                            self.get_expression()?.expect_expression(&self.last_span)?,
                        ))
                    }
                    _ => {
                        else_alias = None;
                        None
                    }
                };

                end_span = match &else_body {
                    Some(else_body) => Some(else_body.span.clone()),
                    None => Some(body.span.clone()),
                };

                expressions::Check {
                    can_be_error,
                    expression_to_check: Box::new(expression_to_check),
                    alias,
                    body: Box::new(body),
                    else_body,
                    else_alias,
                }
                .into()
            }
            TokenEnum::If(_) => {
                self.advance();

                let can_be_error = match self
                    .peek(0)
                    .expect_token(&["QuestionMark", "LeftParen"], &self.last_span)?
                    .expect::<tokens::QuestionMark>()
                {
                    Ok(_) => {
                        self.advance();
                        true
                    }
                    Err(_) => false,
                };

                let mut branches = vec![];
                let mut else_branch = None;

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::LeftParen>()?;
                self.advance();

                let first_condition = self.get_expression()?.expect_expression(&self.last_span)?;

                self.peek(0)
                    .expect_token(&[], &self.last_span)?
                    .expect::<tokens::RightParen>()?;
                self.advance();

                let first_body = self.get_expression()?.expect_expression(&self.last_span)?;

                branches.push((first_condition, first_body));

                while let Some(Token {
                    value: TokenEnum::Else(_),
                    span: _,
                }) = self.peek(0)
                {
                    self.advance();

                    if self
                        .peek(0)
                        .expect_token(&[], &self.last_span)?
                        .expect::<tokens::If>()
                        .is_ok()
                    {
                        self.advance();

                        self.peek(0)
                            .expect_token(&[], &self.last_span)?
                            .expect::<tokens::LeftParen>()?;
                        self.advance();

                        let condition =
                            self.get_expression()?.expect_expression(&self.last_span)?;

                        self.peek(0)
                            .expect_token(&[], &self.last_span)?
                            .expect::<tokens::RightParen>()?;
                        self.advance();

                        let body = self.get_expression()?.expect_expression(&self.last_span)?;

                        branches.push((condition, body));
                    } else {
                        else_branch = Some(Box::new(
                            self.get_expression()?.expect_expression(&self.last_span)?,
                        ));
                    }
                }

                end_span = match &else_branch {
                    Some(else_branch) => Some(else_branch.span.clone()),
                    None => Some(branches.last().unwrap().1.span.clone()),
                };

                expressions::If {
                    can_be_error,
                    branches,
                    else_branch,
                }
                .into()
            }
            TokenEnum::Pipe(_) => {
                self.advance();

                let mut expression_value = vec![];
                let mut identifier_value = vec![];
                let mut same_name_identifier = vec![];

                loop {
                    let token = self.peek(0).expect_token(&["Pipe"], &self.last_span)?;

                    match token {
                        Token {
                            value: TokenEnum::LeftBracket(_),
                            span: _,
                        } => {
                            self.advance();

                            let key = self
                                .get_expression()?
                                .expect_expression(&self.get_current_span())?;

                            self.peek(0)
                                .expect_token(&["RightBracket"], &self.last_span)?
                                .expect::<tokens::RightBracket>()?;
                            self.advance();

                            self.peek(0)
                                .expect_token(&["Equal"], &self.last_span)?
                                .expect::<tokens::Equal>()?;
                            self.advance();

                            let value = self
                                .get_expression()?
                                .expect_expression(&self.get_current_span())?;

                            expression_value.push((key, value));
                        }
                        Token {
                            value: TokenEnum::Identifier(identifier),
                            span: identifier_span,
                        } => {
                            self.advance();

                            let identifier =
                                crate::Identifier::new(&identifier.value, identifier_span, false)?;

                            if self
                                .peek(0)
                                .expect_token(&["Equal", "Pipe", "Comma"], &self.last_span)?
                                .expect::<tokens::Equal>()
                                .is_ok()
                            {
                                self.advance();

                                let value = self
                                    .get_expression()?
                                    .expect_expression(&self.get_current_span())?;

                                identifier_value.push((identifier, value));
                            } else {
                                same_name_identifier.push(identifier);
                            }
                        }
                        Token {
                            value: TokenEnum::Pipe(_),
                            span: _,
                        } => {
                            break;
                        }
                        token => {
                            return Err(Error::new(
                                ErrorType::UnexpectedToken {
                                    expected: vec![
                                        "Pipe".to_string(),
                                        "LeftBracket".to_string(),
                                        "Identifier".to_string(),
                                    ],
                                    actual: token.value.name(),
                                },
                                token.span.clone(),
                            ))
                        }
                    }

                    if self
                        .peek(0)
                        .expect_token(&["Comma", "Pipe"], &self.last_span)?
                        .expect::<tokens::Comma>()
                        .is_ok()
                    {
                        self.advance();
                    } else {
                        self.peek(0)
                            .expect_token(&["Comma", "Pipe"], &self.last_span)?
                            .expect::<tokens::Pipe>()?;

                        break;
                    }
                }

                self.advance();

                end_span = Some(self.get_current_span());

                expressions::Map {
                    expression_value,
                    identifier_value,
                    same_name_identifier,
                }
                .into()
            }
            TokenEnum::LeftBrace(_) => {
                info!(target: TARGET, "now parsing block");

                self.advance();

                let mut statements = vec![];

                loop {
                    let token = self
                        .peek(0)
                        .expect_token(&["RightBrace"], &self.last_span)?;

                    info!(target: TARGET, "token in loop of blocks is '{}'", token);

                    if token.expect::<tokens::RightBrace>().is_ok() {
                        break;
                    }

                    statements.push(
                        self.get_statement()?
                            .expect_statement(&self.get_current_span())?,
                    );
                }

                info!(target: TARGET, "finished parsing block");

                self.advance();

                expressions::Block { statements }.into()
            }
            _ => {
                info!(target: TARGET, "no expression found");
                return Ok(None);
            }
        };

        let span = match end_span {
            Some(end_span) => Span::combine(&start_span, &end_span),
            None => start_span,
        };

        Ok(Some(Expression {
            span,
            value: expression_enum,
        }))
    }

    fn get_current_span(&self) -> Span {
        self.peek(0)
            .map(|t| t.span.clone())
            .unwrap_or(self.last_span.clone())
    }

    fn peek(&self, distance: isize) -> Option<&Token> {
        let index = self.current_index as isize + distance;

        if index < 0 {
            return None;
        }

        let token = self.tokens.get(index as usize);

        let token_string = match token {
            Some(token) => token.to_string(),
            None => "None".to_string(),
        };

        info!(target: TARGET, "Token at distance {} from {} is {}", distance, self.current_index, token_string);

        token
    }

    fn advance(&mut self) {
        self.current_index += 1;
        info!(target: TARGET, "Advancing to token index {}: '{}'", self.current_index, self.tokens.get(self.current_index).map(|v| v.value.to_string()).unwrap_or("EOF".to_string()));
    }
}

#[cfg(test)]
mod tests {
    use crate::Lexer;

    use super::*;
    use tracing_test::traced_test;

    fn get_tracer() -> tracing_subscriber::FmtSubscriber<
        tracing_subscriber::fmt::format::DefaultFields,
        tracing_subscriber::fmt::format::Format,
        tracing_subscriber::EnvFilter,
    > {
        tracing_subscriber::fmt::Subscriber::builder()
            .with_env_filter(TARGET)
            .finish()
    }

    #[test]
    #[traced_test]
    fn import_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let expected = Statement {
            span: Span::new_with_end((1, 1, 0), (1, 35, 34), "test"),
            value: statements::Expression {
                expression: Box::new(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 34, 33), "test"),
                    value: expressions::Assignment {
                        identifier: crate::Identifier::new(
                            "foo",
                            Span::new_with_end((1, 1, 0), (1, 3, 2), "test"),
                            false,
                        )
                        .unwrap(),
                        expression: Box::new(Expression {
                            span: Span::new_with_end((1, 7, 6), (1, 34, 33), "test"),
                            value: expressions::Addition {
                                lhs: Box::new(Expression {
                                    span: Span::new_with_end((1, 7, 6), (1, 30, 29), "test"),
                                    value: expressions::Group {
                                        value: Box::new(Expression {
                                            span: Span::new_with_end(
                                                (1, 8, 7),
                                                (1, 29, 28),
                                                "test",
                                            ),
                                            value: expressions::Addition {
                                                lhs: Box::new(Expression {
                                                    span: Span::new_with_end(
                                                        (1, 8, 7),
                                                        (1, 25, 24),
                                                        "test",
                                                    ),
                                                    value: expressions::FieldAccess {
                                                        expression: Box::new(Expression {
                                                            span: Span::new_with_end(
                                                                (1, 8, 7),
                                                                (1, 20, 19),
                                                                "test",
                                                            ),
                                                            value: expressions::Call {
                                                                expression: Box::new(Expression {
                                                                    span: Span::new_with_end(
                                                                        (1, 8, 7),
                                                                        (1, 13, 12),
                                                                        "test",
                                                                    ),
                                                                    value:
                                                                        expressions::Identifier {
                                                                            value: "import"
                                                                                .to_string(),
                                                                        }
                                                                        .into(),
                                                                }),
                                                                arguments: vec![Expression {
                                                                    span: Span::new_with_end(
                                                                        (1, 15, 14),
                                                                        (1, 19, 18),
                                                                        "test",
                                                                    ),
                                                                    value: expressions::Str {
                                                                        value: "std".to_string(),
                                                                    }
                                                                    .into(),
                                                                }],
                                                            }
                                                            .into(),
                                                        }),
                                                        field: crate::Identifier::new(
                                                            "argc",
                                                            Span::new_with_end(
                                                                (1, 22, 21),
                                                                (1, 25, 24),
                                                                "test",
                                                            ),
                                                            false,
                                                        )
                                                        .unwrap(),
                                                    }
                                                    .into(),
                                                }),
                                                rhs: Box::new(Expression {
                                                    span: Span::new((1, 29, 28), "test"),
                                                    value: expressions::Number { value: 2.0 }
                                                        .into(),
                                                }),
                                            }
                                            .into(),
                                        }),
                                    }
                                    .into(),
                                }),
                                rhs: Box::new(Expression {
                                    span: Span::new((1, 34, 33), "test"),
                                    value: expressions::Number { value: 2.0 }.into(),
                                }),
                            }
                            .into(),
                        }),
                    }
                    .into(),
                }),
            }
            .into(),
        };

        test_parser("foo = (import('std').argc + 2) + 2;", &[expected]);
    }

    #[test]
    #[traced_test]
    fn field_access_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let expected = Statement {
            span: Span::new_with_end((1, 1, 0), (1, 8, 7), "test"),
            value: statements::Expression {
                expression: Box::new(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 7, 6), "test"),
                    value: expressions::FieldAccess {
                        expression: Box::new(Expression {
                            span: Span::new_with_end((1, 1, 0), (1, 3, 2), "test"),
                            value: expressions::Identifier {
                                value: "foo".to_string(),
                            }
                            .into(),
                        }),
                        field: crate::Identifier::new(
                            "bar",
                            Span::new_with_end((1, 5, 4), (1, 7, 6), "test"),
                            false,
                        )
                        .unwrap(),
                    }
                    .into(),
                }),
            }
            .into(),
        };

        test_parser("foo.bar;", &[expected]);
    }

    #[test]
    #[traced_test]
    fn index_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let expected = Statement {
            span: Span::new_with_end((1, 1, 0), (1, 7, 6), "test"),
            value: statements::Expression {
                expression: Box::new(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 6, 5), "test"),
                    value: expressions::Index {
                        expression: Box::new(Expression {
                            span: Span::new_with_end((1, 1, 0), (1, 3, 2), "test"),
                            value: expressions::Identifier {
                                value: "foo".to_string(),
                            }
                            .into(),
                        }),
                        index: Box::new(Expression {
                            span: Span::new((1, 5, 4), "test"),
                            value: expressions::Number { value: 1.0 }.into(),
                        }),
                    }
                    .into(),
                }),
            }
            .into(),
        };

        test_parser("foo[1];", &[expected]);
    }

    #[test]
    #[traced_test]
    fn calls_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let expected = Statement {
            span: Span::new_with_end((1, 1, 0), (1, 15, 14), "test"),
            value: statements::Expression {
                expression: Box::new(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 14, 13), "test"),
                    value: expressions::Call {
                        expression: Box::new(Expression {
                            span: Span::new_with_end((1, 1, 0), (1, 3, 2), "test"),
                            value: expressions::Identifier {
                                value: "foo".to_string(),
                            }
                            .into(),
                        }),
                        arguments: vec![Expression {
                            span: Span::new_with_end((1, 5, 4), (1, 13, 12), "test"),
                            value: expressions::Call {
                                expression: Box::new(Expression {
                                    span: Span::new_with_end((1, 5, 4), (1, 7, 6), "test"),
                                    value: expressions::Identifier {
                                        value: "bar".to_string(),
                                    }
                                    .into(),
                                }),
                                arguments: vec![],
                            }
                            .into(),
                        }],
                    }
                    .into(),
                }),
            }
            .into(),
        };

        test_parser("foo(bar(    ));", &[expected]);
    }

    #[test]
    #[traced_test]
    fn ranges_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let expected = Statement {
            span: Span::new_with_end((1, 1, 0), (1, 5, 4), "test"),
            value: statements::Expression {
                expression: Box::new(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 4, 3), "test"),
                    value: expressions::Range::Closed {
                        lhs: Box::new(Expression {
                            span: Span::new((1, 1, 0), "test"),
                            value: expressions::Number { value: 1.0 }.into(),
                        }),
                        rhs: Box::new(Expression {
                            span: Span::new((1, 4, 3), "test"),
                            value: expressions::Number { value: 3.0 }.into(),
                        }),
                    }
                    .into(),
                }),
            }
            .into(),
        };

        test_parser("1..3;", &[expected]);
    }

    #[test]
    #[traced_test]
    fn assignment_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let expected = Statement {
            span: Span::new_with_end((1, 1, 0), (1, 14, 13), "test"),
            value: statements::Expression {
                expression: Box::new(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 13, 12), "test"),
                    value: expressions::Assignment {
                        identifier: crate::Identifier::new(
                            "foo",
                            Span::new_with_end((1, 1, 0), (1, 3, 2), "test"),
                            false,
                        )
                        .unwrap(),
                        expression: Box::new(Expression {
                            span: Span::new_with_end((1, 7, 6), (1, 13, 12), "test"),
                            value: expressions::Or {
                                lhs: Box::new(Expression {
                                    span: Span::new_with_end((1, 7, 6), (1, 8, 7), "test"),
                                    value: expressions::Number { value: 12.0 }.into(),
                                }),
                                rhs: Box::new(Expression {
                                    span: Span::new((1, 13, 12), "test"),
                                    value: expressions::Number { value: 3.0 }.into(),
                                }),
                            }
                            .into(),
                        }),
                    }
                    .into(),
                }),
            }
            .into(),
        };

        test_parser("foo = 12 or 3;", &[expected]);
    }

    #[test]
    #[traced_test]
    fn unary_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let expected = Statement {
            span: Span::new_with_end((1, 1, 0), (1, 6, 5), "test"),
            value: statements::Expression {
                expression: Box::new(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 5, 4), "test"),
                    value: expressions::Not {
                        expression: Box::new(Expression {
                            span: Span::new_with_end((1, 2, 1), (1, 5, 4), "test"),
                            value: expressions::Negate {
                                expression: Box::new(Expression {
                                    span: Span::new_with_end((1, 3, 2), (1, 5, 4), "test"),
                                    value: expressions::Identifier {
                                        value: "foo".to_string(),
                                    }
                                    .into(),
                                }),
                            }
                            .into(),
                        }),
                    }
                    .into(),
                }),
            }
            .into(),
        };

        test_parser("!-foo;", &[expected]);
    }

    fn test_parser(source: &str, expected: &[Statement]) {
        let tokens = Lexer::new("test", source).get_tokens().unwrap_fmt();

        let actual = Parser::new("test", tokens.as_slice())
            .get_statements()
            .unwrap_fmt();

        if expected != actual {
            println!("Expected:");
            for expected in expected {
                println!("{}", crate::display_tree::display_statement(expected));
            }
            println!("\nActual:");
            for actual in &actual {
                println!("{}", crate::display_tree::display_statement(actual));
            }
            assert_eq!(expected, actual);
        }
    }
}
