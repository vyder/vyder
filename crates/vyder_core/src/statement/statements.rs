use crate::prelude::*;

#[derive(Clone, Debug, IntoStatementEnum, PartialEq)]
pub struct Expression {
    pub expression: Box<crate::Expression>,
}

#[derive(Clone, Debug, IntoStatementEnum, PartialEq)]
pub struct Break;

#[derive(Clone, Debug, IntoStatementEnum, PartialEq)]
pub struct Continue;

#[derive(Clone, Debug, IntoStatementEnum, PartialEq)]
pub struct Return {
    pub expression: Option<Box<crate::Expression>>,
}

#[derive(Clone, Debug, IntoStatementEnum, PartialEq)]
pub struct Exit {
    pub expression: Option<Box<crate::Expression>>,
}

#[derive(Clone, Debug, IntoStatementEnum, PartialEq)]
pub struct Ev {
    pub expression: Option<Box<crate::Expression>>,
}

#[derive(Clone, Debug, IntoStatementEnum, PartialEq)]
pub struct Declaration {
    pub is_const: bool,
    pub identifier: crate::Identifier,
    pub expression: Box<crate::Expression>,
}
