use tracing::info;

use crate::{prelude::*, Lexer};

mod control_flow;
pub use control_flow::ControlFlow;

#[allow(unused)]
pub const TARGET: &str = "vyder_core::interpreter";

pub struct Interpreter {
    pub environment: Environment,
    work_dir: String,
    modules: Vec<(String, ValueEnum)>,
    module_name: String,
}

impl Interpreter {
    pub fn new(
        module_name: impl Into<String>,
        work_dir: &str,
        modules: Vec<(String, ValueEnum)>,
    ) -> Self {
        Self {
            module_name: module_name.into(),
            environment: Environment::new(),
            work_dir: work_dir.to_string(),
            modules,
        }
    }

    pub fn interpret_statements(&mut self, statements: Vec<Statement>) -> Result<ControlFlow> {
        for statement in statements {
            info!(target: TARGET, "interpreting statement {}", statement);
            let control_flow = self.interpret_statement(statement)?;
            info!(target: TARGET, "got {:?}", control_flow);

            match control_flow {
                ControlFlow::None | ControlFlow::Normal(_) => {}
                _ => return Ok(control_flow),
            };
        }

        Ok(ControlFlow::None)
    }

    pub fn interpret_statement(&mut self, statement: Statement) -> Result<ControlFlow> {
        match statement.value {
            StatementEnum::Expression(expression) => {
                self.evaluate_expression(*expression.expression)
            }
            StatementEnum::Declaration(declaration) => {
                let control_flow = self.evaluate_expression(*declaration.expression)?;
                let value = match control_flow {
                    ControlFlow::Normal(value) => value,
                    _ => return Ok(control_flow),
                };
                self.environment
                    .define(declaration.identifier.name(), value, declaration.is_const);

                Ok(ControlFlow::None)
            }
            StatementEnum::Break(_) => Ok(ControlFlow::Break),
            StatementEnum::Continue(_) => Ok(ControlFlow::Continue),
            StatementEnum::Return(return_statement) => match return_statement.expression {
                Some(expression) => Ok(ControlFlow::Return(
                    match self.evaluate_expression(*expression)? {
                        ControlFlow::Return(value) | ControlFlow::Normal(value) => value,
                        ControlFlow::Ev(_) => unreachable!("because this is an expression, an Ev control flow should be handled by a Block"),
                        control_flow => return Ok(control_flow),
                    },
                )),
                None => Ok(ControlFlow::Return(Value::new_ok_value(
                    values::Nil.into(),
                    statement.span,
                ))),
            },
            StatementEnum::Ev(ev_statement) => match ev_statement.expression {
                Some(expression) => {
                    let control_flow = self.evaluate_expression(*expression)?;
                    match control_flow {
                        ControlFlow::Normal(value) => Ok(ControlFlow::Ev(value)),
                        ControlFlow::None => Ok(ControlFlow::Ev(Value::new_ok_value(values::Nil.into(), statement.span))),
                        ControlFlow::Ev(_) => unreachable!("because this is an expression, an Ev control flow should be handled by a Block"),
                        control_flow => Ok(control_flow),
                    }
                }
                None => Ok(ControlFlow::Ev(Value::new_ok_value(
                    values::Nil.into(),
                    statement.span,
                ))),
            },
            StatementEnum::Exit(exit_statement) => match exit_statement.expression {
                Some(expression) => {
                    let control_flow = self.evaluate_expression(*expression)?;
                    match control_flow {
                        ControlFlow::Normal(value) => Ok(ControlFlow::Exit((value.expect_non_error()?.expect_integer()? as u8).into())),
                        ControlFlow::None => Ok(ControlFlow::Exit(0.into())),
                        ControlFlow::Ev(_) => unreachable!("because this is an expression, an Ev control flow should be handled by a Block"),
                        control_flow => Ok(control_flow),
                    }
                }
                None => Ok(ControlFlow::Exit(0.into())),
            },
        }
    }

    #[allow(clippy::only_used_in_recursion)]
    pub fn evaluate_expression(&mut self, expression: Expression) -> Result<ControlFlow> {
        match expression.value {
            ExpressionEnum::Number(number) => Ok(ControlFlow::Normal(Value::new_ok_value(
                values::Number {
                    value: number.value,
                }
                .into(),
                expression.span,
            ))),
            ExpressionEnum::Str(string) => Ok(ControlFlow::Normal(Value::new_ok_value(
                values::Str {
                    value: string.value,
                }
                .into(),
                expression.span,
            ))),
            ExpressionEnum::True(_) => Ok(ControlFlow::Normal(Value::new_ok_value(
                values::Bool { value: true }.into(),
                expression.span,
            ))),
            ExpressionEnum::False(_) => Ok(ControlFlow::Normal(Value::new_ok_value(
                values::Bool { value: false }.into(),
                expression.span,
            ))),
            ExpressionEnum::Nil(_) => Ok(ControlFlow::Normal(Value::new_ok_value(
                values::Nil.into(),
                expression.span,
            ))),
            ExpressionEnum::Range(range) => match range {
                expressions::Range::Closed { rhs, lhs } => {
                    Ok(ControlFlow::Normal(Value::new_ok_value(
                        values::Range::Closed {
                            lhs: match self.evaluate_expression(*lhs.clone())? {
                                ControlFlow::Normal(value) => {
                                    value.expect_non_error()?.expect_integer()?
                                }
                                control_flow => return Ok(control_flow),
                            },
                            rhs: match self.evaluate_expression(*rhs.clone())? {
                                ControlFlow::Normal(value) => {
                                    value.expect_non_error()?.expect_integer()?
                                }
                                control_flow => return Ok(control_flow),
                            },
                        }
                        .into(),
                        expression.span,
                    )))
                }
                expressions::Range::From { lhs } => Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Range::From {
                        lhs: match self.evaluate_expression(*lhs.clone())? {
                            ControlFlow::Normal(value) => {
                                value.expect_non_error()?.expect_integer()?
                            }
                            control_flow => return Ok(control_flow),
                        },
                    }
                    .into(),
                    expression.span,
                ))),
                expressions::Range::To { rhs } => Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Range::To {
                        rhs: match self.evaluate_expression(*rhs.clone())? {
                            ControlFlow::Normal(value) => {
                                value.expect_non_error()?.expect_integer()?
                            }
                            control_flow => return Ok(control_flow),
                        },
                    }
                    .into(),
                    expression.span,
                ))),
            },
            ExpressionEnum::Group(group) => {
                let mut value = match self.evaluate_expression(*group.value)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                };
                value.inner_mut().span = expression.span;
                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::Map(map) => {
                let mut final_map = values::Map { values: vec![] };

                for (expression, value) in map.expression_value {
                    let mut key_result = self.evaluate_expression(expression)?;
                    let key = match key_result {
                        ControlFlow::Normal(ref mut value) => &value.inner_mut().value,
                        control_flow => return Ok(control_flow),
                    };

                    let mut control_flow = self.evaluate_expression(value)?;
                    let value = match control_flow {
                        ControlFlow::Normal(ref mut value) => &value.inner_mut().value,
                        control_flow => return Ok(control_flow),
                    };

                    final_map.values.push((key.clone(), value.clone()));
                }

                for (identifier, value) in map.identifier_value {
                    let key = values::Str {
                        value: identifier.name().clone(),
                    }
                    .into();

                    let mut control_flow = self.evaluate_expression(value)?;
                    let value = match control_flow {
                        ControlFlow::Normal(ref mut value) => &value.inner_mut().value,
                        control_flow => return Ok(control_flow),
                    };

                    final_map.values.push((key, value.clone()));
                }

                for identifier in map.same_name_identifier {
                    let key = values::Str {
                        value: identifier.name().clone(),
                    }
                    .into();
                    let mut control_flow = self.evaluate_expression(Expression {
                        value: expressions::Identifier {
                            value: identifier.name().clone(),
                        }
                        .into(),
                        span: identifier.span().clone(),
                    })?;

                    let value = match control_flow {
                        ControlFlow::Normal(ref mut value) => &value.inner_mut().value,
                        control_flow => return Ok(control_flow),
                    };

                    final_map.values.push((key, value.clone()));
                }

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    final_map.into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Negate(negate) => {
                let value = match self.evaluate_expression(*negate.expression)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?.value
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Number { value: -value }.into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Not(not) => {
                info!(target: TARGET, "evaluating 'not'");
                let value = match self.evaluate_expression(*not.expression)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Bool>()?.value
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool { value: !value }.into(),
                    expression.span,
                )))
            }
            ExpressionEnum::And(and) => {
                info!(target: TARGET, "evaluating 'and'");

                let lhs = match self.evaluate_expression(*and.lhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*and.rhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };

                let lhs = lhs.expect::<values::Bool>()?.value;
                let rhs = rhs.expect::<values::Bool>()?.value;

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool { value: lhs && rhs }.into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Or(or) => {
                info!(target: TARGET, "evaluating 'and'");
                let lhs = match self.evaluate_expression(*or.lhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*or.rhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };

                let lhs = lhs.expect::<values::Bool>()?.value;
                let rhs = rhs.expect::<values::Bool>()?.value;

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool { value: lhs || rhs }.into(),
                    expression.span,
                )))
            }
            ExpressionEnum::AsError(as_error) => {
                let expression_result = self.evaluate_expression(*as_error.expression)?;
                let mut value = match expression_result {
                    ControlFlow::Normal(ref value) => value.ignore_error(),
                    control_flow => return Ok(control_flow),
                }
                .clone();
                value.span = expression.span;
                let value = ValueResult::Err(value.clone());
                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::Equal(equal) => {
                let lhs = match self.evaluate_expression(*equal.lhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*equal.rhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool {
                        value: lhs.value == rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::NotEqual(not_equal) => {
                let lhs = match self.evaluate_expression(*not_equal.lhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*not_equal.rhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool {
                        value: lhs.value != rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Less(less) => {
                let lhs = match self.evaluate_expression(*less.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*less.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool {
                        value: lhs.value < rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::LessEqual(less_equal) => {
                let lhs = match self.evaluate_expression(*less_equal.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*less_equal.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool {
                        value: lhs.value <= rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Greater(greater) => {
                let lhs = match self.evaluate_expression(*greater.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*greater.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool {
                        value: lhs.value > rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::GreaterEqual(greater_equal) => {
                let lhs = match self.evaluate_expression(*greater_equal.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };
                let rhs = match self.evaluate_expression(*greater_equal.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Bool {
                        value: lhs.value >= rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Addition(addition) => {
                let lhs = match self.evaluate_expression(*addition.lhs)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };

                match lhs.value {
                    ValueEnum::Number(number) => {
                        let rhs = match self.evaluate_expression(*addition.rhs)? {
                            ControlFlow::Normal(value) => {
                                value.expect_non_error()?.expect::<values::Number>()?
                            }
                            control_flow => return Ok(control_flow),
                        };

                        Ok(ControlFlow::Normal(Value::new_ok_value(
                            values::Number {
                                value: number.value + rhs.value,
                            }
                            .into(),
                            expression.span,
                        )))
                    }
                    ValueEnum::Str(string) => {
                        let rhs = match self.evaluate_expression(*addition.rhs)? {
                            ControlFlow::Normal(value) => {
                                value.expect_non_error()?.expect::<values::Str>()?
                            }
                            control_flow => return Ok(control_flow),
                        };

                        Ok(ControlFlow::Normal(Value::new_ok_value(
                            values::Str {
                                value: format!("{}{}", string.value, rhs.value),
                            }
                            .into(),
                            expression.span,
                        )))
                    }
                    value => Err(Error::new(
                        ErrorType::UnexpectedValue {
                            expected: vec!["Number".to_string(), "String".to_string()],
                            actual: value.name(),
                        },
                        lhs.span,
                    )),
                }
            }
            ExpressionEnum::Subtraction(subtraction) => {
                let lhs = match self.evaluate_expression(*subtraction.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                let rhs = match self.evaluate_expression(*subtraction.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Number {
                        value: lhs.value - rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Multiplication(multiplication) => {
                let lhs = match self.evaluate_expression(*multiplication.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                let rhs = match self.evaluate_expression(*multiplication.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Number {
                        value: lhs.value * rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Division(division) => {
                let lhs = match self.evaluate_expression(*division.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                let rhs = match self.evaluate_expression(*division.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                if rhs.value == 0.0 {
                    return Err(Error::new(ErrorType::DivisionByZero, expression.span));
                }

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Number {
                        value: lhs.value / rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Modulo(modulo) => {
                let lhs = match self.evaluate_expression(*modulo.lhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                let rhs = match self.evaluate_expression(*modulo.rhs)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Number>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                if rhs.value == 0.0 {
                    return Err(Error::new(ErrorType::ModuloByZero, expression.span));
                }

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    values::Number {
                        value: lhs.value % rhs.value,
                    }
                    .into(),
                    expression.span,
                )))
            }
            ExpressionEnum::Index(index) => {
                let to_index = match self.evaluate_expression(*index.expression)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Map>()?
                    }
                    control_flow => return Ok(control_flow),
                };
                let index = match self.evaluate_expression(*index.index)? {
                    ControlFlow::Normal(value) => value.expect_non_error()?,
                    control_flow => return Ok(control_flow),
                };

                for (key, value) in to_index.values {
                    if index.value == key {
                        return Ok(ControlFlow::Normal(Value::new_ok_value(
                            value,
                            expression.span,
                        )));
                    }
                }

                Err(Error::new(
                    ErrorType::InvalidKey(index.value.to_string()),
                    index.span,
                ))
            }
            ExpressionEnum::FieldAccess(field_access) => {
                let to_access = match self.evaluate_expression(*field_access.expression)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Map>()?
                    }
                    control_flow => return Ok(control_flow),
                };
                let field = field_access.field;

                for (key, value) in to_access.values {
                    if let ValueEnum::Str(key) = key {
                        if key.value == *field.name() {
                            return Ok(ControlFlow::Normal(Value::new_ok_value(
                                value,
                                expression.span,
                            )));
                        }
                    }
                }

                Err(Error::new(
                    ErrorType::InvalidKey(field.name().clone()),
                    field.span().clone(),
                ))
            }
            ExpressionEnum::Function(function) => Ok(ControlFlow::Normal(Value::new_ok_value(
                values::Callable::User {
                    parameters: function.parameters,
                    body: function.body,
                }
                .into(),
                expression.span,
            ))),
            ExpressionEnum::Call(call) => {
                if let Ok(identifier) = call.expression.expect::<expressions::Identifier>() {
                    if identifier.value == *"import" {
                        if call.arguments.len() != 1 {
                            return Err(Error::new(
                                ErrorType::UnexpectedAmountOfArguments {
                                    expected: 1,
                                    actual: call.arguments.len(),
                                },
                                expression.span,
                            ));
                        };

                        let file_path = match self
                            .evaluate_expression(call.arguments.into_iter().next().unwrap())?
                        {
                            ControlFlow::Normal(value) => {
                                value.expect_non_error()?.expect::<values::Str>()?.value
                            }
                            control_flow => return Ok(control_flow),
                        };

                        for (identifier, module) in &self.modules {
                            if identifier == &file_path {
                                return Ok(ControlFlow::Normal(Value::new_ok_value(
                                    module.clone(),
                                    expression.span,
                                )));
                            }
                        }

                        let file_path = format!("{}/{}", self.work_dir, file_path);

                        let source = match std::fs::read_to_string(file_path.clone()) {
                            Ok(source) => source,
                            Err(_e) => {
                                return Err(Error::new(
                                    ErrorType::ModuleNotFound(file_path),
                                    expression.span,
                                ))
                            }
                        };

                        let mut lexer = Lexer::new(&file_path, &source);
                        let tokens = lexer.get_tokens()?;

                        let mut parser = Parser::new("test", &tokens);
                        let statements = parser.get_statements()?;

                        let mut interpreter =
                            Interpreter::new(file_path, &self.work_dir, self.modules.clone());
                        let control_flow = interpreter.interpret_statements(statements)?;

                        let value = match control_flow {
                            ControlFlow::Return(value) | ControlFlow::Ev(value) => value,
                            control_flow @ ControlFlow::Exit(_) => return Ok(control_flow),
                            _ => {
                                return Ok(ControlFlow::Normal(Value::new_ok_value(
                                    values::Nil.into(),
                                    expression.span,
                                )))
                            }
                        };

                        return Ok(ControlFlow::Normal(Value::new_ok_value(
                            value.ignore_error().value.clone(),
                            expression.span,
                        )));
                    }
                }

                let function_to_call = match self.evaluate_expression(*call.expression)? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Callable>()?
                    }
                    control_flow => return Ok(control_flow),
                };

                match function_to_call {
                    values::Callable::User { parameters, body } => {
                        self.environment.enter_scope();

                        for (parameter, argument) in std::iter::zip(parameters, call.arguments) {
                            let argument = match self.evaluate_expression(argument)? {
                                ControlFlow::Normal(value) => value,
                                control_flow => return Ok(control_flow),
                            };

                            self.environment.define(parameter.name(), argument, false)
                        }

                        let snippet = body.span.get_snippet().clone();

                        let mut value = match self.evaluate_expression(*body)? {
                            ControlFlow::Normal(value) | ControlFlow::Return(value) => value,
                            control_flow @ ControlFlow::Exit(_) => return Ok(control_flow),
                            _ => ValueResult::Ok(Value::new_value(
                                values::Nil.into(),
                                Span::new((1, 1, 0), self.module_name.clone()).set_snippet(snippet), // will be overwritten
                            )),
                        };

                        value.inner_mut().span = expression.span;

                        self.environment.exit_scope();

                        Ok(ControlFlow::Normal(value))
                    }
                    values::Callable::Lib {
                        function,
                        can_be_error: _,
                    } => {
                        let mut arguments = vec![];

                        for argument in call.arguments {
                            let argument = match self.evaluate_expression(argument)? {
                                ControlFlow::Normal(value) => value,
                                control_flow => return Ok(control_flow),
                            };

                            arguments.push(argument);
                        }

                        let (value, do_exit) = function(&arguments, expression.span)?;

                        Ok(match do_exit {
                            Some(code) => ControlFlow::Exit(code),
                            None => ControlFlow::Normal(value),
                        })
                    }
                }
            }
            ExpressionEnum::If(if_expression) => {
                info!(target: TARGET, "evaluating 'if'");

                for (condition, body) in if_expression.branches {
                    let condition = match self.evaluate_expression(condition)? {
                        ControlFlow::Normal(value) => {
                            value.expect_non_error()?.expect::<values::Bool>()?
                        }
                        control_flow => return Ok(control_flow),
                    };

                    if condition.value {
                        return self.evaluate_expression(body);
                    }
                }

                match if_expression.else_branch {
                    Some(else_branch) => self.evaluate_expression(*else_branch),
                    None => Ok(ControlFlow::Normal(Value::new_ok_value(
                        values::Nil.into(),
                        expression.span,
                    ))),
                }
            }
            ExpressionEnum::Check(check) => {
                self.environment.enter_scope();

                let value_to_check = match self.evaluate_expression(*check.expression_to_check)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                };

                let result = match value_to_check {
                    ValueResult::Ok(_) => {
                        if let Some(alias) = check.alias {
                            self.environment
                                .define(alias.name(), value_to_check.clone(), false);
                        };
                        self.evaluate_expression(*check.body)
                    }
                    ValueResult::Err(_) => match check.else_body {
                        Some(else_body) => {
                            if let Some(alias) = check.else_alias {
                                self.environment.define(
                                    alias.name(),
                                    value_to_check.clone(),
                                    false,
                                );
                            };
                            self.evaluate_expression(*else_body)
                        }
                        None => Ok(ControlFlow::Normal(Value::new_ok_value(
                            values::Nil.into(),
                            expression.span,
                        ))),
                    },
                };

                self.environment.exit_scope();

                result
            }
            ExpressionEnum::Block(block) => {
                self.environment.enter_scope();

                let control_flow = self.interpret_statements(block.statements)?;

                let value = match control_flow {
                    ControlFlow::Ev(value) => value,
                    ControlFlow::Return(value) => return Ok(ControlFlow::Return(value)),
                    ControlFlow::Normal(_) | ControlFlow::None => {
                        info!(target: TARGET, "constructing nil");
                        Value::new_ok_value(values::Nil.into(), expression.span)
                    }
                    _ => return Ok(control_flow),
                };

                info!(target: TARGET, "block evaluates to {:?}", value);

                self.environment.exit_scope();

                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::Identifier(identifier) => Ok(ControlFlow::Normal(
                self.environment
                    .get(&identifier.value, &expression.span)?
                    .clone(),
            )),
            ExpressionEnum::Assignment(assignment) => {
                let value = match self.evaluate_expression(*assignment.expression)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                };

                self.environment.assign(
                    assignment.identifier.name(),
                    value.clone(),
                    &expression.span,
                )?;

                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::AdditionAssignment(addition_assignment) => {
                let value = match self
                    .environment
                    .get(
                        addition_assignment.identifier.name(),
                        addition_assignment.identifier.span(),
                    )?
                    .clone()
                    .expect_non_error()?
                    .value
                {
                    ValueEnum::Str(lhs) => {
                        let rhs =
                            match self.evaluate_expression(*addition_assignment.expression)? {
                                ControlFlow::Normal(value) => value,
                                result => return Ok(result),
                            }
                            .expect_non_error()?
                            .expect::<values::Str>()?;

                        values::Str {
                            value: format!("{}{}", lhs.value, rhs.value),
                        }
                        .into()
                    }
                    ValueEnum::Number(lhs) => {
                        let rhs =
                            match self.evaluate_expression(*addition_assignment.expression)? {
                                ControlFlow::Normal(value) => value,
                                control_flow => return Ok(control_flow),
                            }
                            .expect_non_error()?
                            .expect::<values::Number>()?;

                        values::Number {
                            value: lhs.value + rhs.value,
                        }
                        .into()
                    }
                    value => {
                        return Err(Error::new(
                            ErrorType::UnexpectedValue {
                                expected: vec!["Str".to_string(), "Number".to_string()],
                                actual: value.name(),
                            },
                            expression.span,
                        ))
                    }
                };

                let value = Value::new_ok_value(value, expression.span.clone());

                self.environment.assign(
                    addition_assignment.identifier.name(),
                    value.clone(),
                    &expression.span,
                )?;

                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::SubtractionAssignment(subtraction_assignment) => {
                let rhs = match self.evaluate_expression(*subtraction_assignment.expression)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                }
                .expect_non_error()?
                .expect::<values::Number>()?;

                let lhs = self
                    .environment
                    .get(
                        subtraction_assignment.identifier.name(),
                        subtraction_assignment.identifier.span(),
                    )?
                    .clone()
                    .expect_non_error()?
                    .expect::<values::Number>()?;

                let value = Value::new_ok_value(
                    values::Number {
                        value: lhs.value - rhs.value,
                    }
                    .into(),
                    expression.span.clone(),
                );

                self.environment.assign(
                    subtraction_assignment.identifier.name(),
                    value.clone(),
                    &expression.span,
                )?;

                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::MultiplicationAssignment(multiplication_assignment) => {
                let rhs = match self.evaluate_expression(*multiplication_assignment.expression)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                }
                .expect_non_error()?
                .expect::<values::Number>()?;

                let lhs = self
                    .environment
                    .get(
                        multiplication_assignment.identifier.name(),
                        multiplication_assignment.identifier.span(),
                    )?
                    .clone()
                    .expect_non_error()?
                    .expect::<values::Number>()?;

                let value = Value::new_ok_value(
                    values::Number {
                        value: lhs.value * rhs.value,
                    }
                    .into(),
                    expression.span.clone(),
                );

                self.environment.assign(
                    multiplication_assignment.identifier.name(),
                    value.clone(),
                    &expression.span,
                )?;

                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::DivisionAssignment(division_assignment) => {
                let rhs = match self.evaluate_expression(*division_assignment.expression)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                }
                .expect_non_error()?
                .expect::<values::Number>()?;

                if rhs.value == 0.0 {
                    return Err(Error::new(ErrorType::DivisionByZero, expression.span));
                };

                let lhs = self
                    .environment
                    .get(
                        division_assignment.identifier.name(),
                        division_assignment.identifier.span(),
                    )?
                    .clone()
                    .expect_non_error()?
                    .expect::<values::Number>()?;

                let value = Value::new_ok_value(
                    values::Number {
                        value: lhs.value / rhs.value,
                    }
                    .into(),
                    expression.span.clone(),
                );

                self.environment.assign(
                    division_assignment.identifier.name(),
                    value.clone(),
                    &expression.span,
                )?;

                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::ModuloAssignment(modulo_assignment) => {
                let rhs = match self.evaluate_expression(*modulo_assignment.expression)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                }
                .expect_non_error()?
                .expect::<values::Number>()?;

                if rhs.value == 0.0 {
                    return Err(Error::new(ErrorType::ModuloByZero, expression.span));
                };

                let lhs = self
                    .environment
                    .get(
                        modulo_assignment.identifier.name(),
                        modulo_assignment.identifier.span(),
                    )?
                    .clone()
                    .expect_non_error()?
                    .expect::<values::Number>()?;

                let value = Value::new_ok_value(
                    values::Number {
                        value: lhs.value % rhs.value,
                    }
                    .into(),
                    expression.span.clone(),
                );

                self.environment.assign(
                    modulo_assignment.identifier.name(),
                    value.clone(),
                    &expression.span,
                )?;

                Ok(ControlFlow::Normal(value))
            }
            ExpressionEnum::While(while_expression) => {
                info!(target: TARGET, "evaluating 'while'");

                self.environment.enter_scope();

                let mut value: ValueEnum = values::Nil.into();
                while match self.evaluate_expression(*while_expression.condition.clone())? {
                    ControlFlow::Normal(value) => {
                        value.expect_non_error()?.expect::<values::Bool>()?.value
                    }
                    control_flow => return Ok(control_flow),
                } {
                    match self.evaluate_expression(*while_expression.body.clone())? {
                        ControlFlow::Normal(value_of_body) => match value_of_body {
                            ValueResult::Ok(value_of_body)
                                if value_of_body.expect::<values::Nil>().is_err() =>
                            {
                                value = value_of_body.value;
                                break;
                            }
                            ValueResult::Err(value_of_body) => {
                                value = value_of_body.value;
                                break;
                            }
                            _ => {}
                        },
                        ControlFlow::Continue => {}
                        ControlFlow::Break => break,
                        control_flow => return Ok(control_flow),
                    }
                }

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    value,
                    expression.span,
                )))
            }
            ExpressionEnum::For(for_expression) => {
                self.environment.enter_scope();

                let mut value: ValueEnum = values::Nil.into();

                let value_to_loop_over =
                    match self.evaluate_expression(*for_expression.to_loop_over)? {
                        ControlFlow::Normal(value) => value
                            .expect_non_error()?
                            .value
                            .get_iterator(&expression.span)?,
                        control_flow => return Ok(control_flow),
                    };

                info!(target: TARGET, "looping over {:?}", value_to_loop_over);

                for value_of_for in value_to_loop_over {
                    self.environment.define(
                        for_expression.identifier.name(),
                        ValueResult::Ok(value_of_for),
                        false,
                    );

                    match self.evaluate_expression(*for_expression.body.clone())? {
                        ControlFlow::Normal(value_of_body) => match value_of_body {
                            ValueResult::Ok(value_of_body)
                                if value_of_body.expect::<values::Nil>().is_err() =>
                            {
                                value = value_of_body.value;
                                break;
                            }
                            ValueResult::Err(value_of_body) => {
                                value = value_of_body.value;
                                break;
                            }
                            _ => {}
                        },
                        ControlFlow::Continue => {}
                        ControlFlow::Break => break,
                        result => return Ok(result),
                    };
                }

                self.environment.exit_scope();

                Ok(ControlFlow::Normal(Value::new_ok_value(
                    value,
                    expression.span,
                )))
            }
            ExpressionEnum::Expect(expect) => {
                let value = match self.evaluate_expression(*expect.expression)? {
                    ControlFlow::Normal(value) => value,
                    control_flow => return Ok(control_flow),
                };

                match value {
                    ValueResult::Ok(value) => Ok(ControlFlow::Normal(ValueResult::Ok(value))),
                    ValueResult::Err(value) => Ok(ControlFlow::Return(ValueResult::Err(value))),
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use tracing_test::traced_test;

    use super::*;

    fn get_tracer() -> tracing_subscriber::FmtSubscriber<
        tracing_subscriber::fmt::format::DefaultFields,
        tracing_subscriber::fmt::format::Format,
        tracing_subscriber::EnvFilter,
    > {
        tracing_subscriber::fmt::Subscriber::builder()
            .with_env_filter(TARGET)
            .finish()
    }

    #[test]
    #[traced_test]
    fn number_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let mut interpreter = Interpreter::new("test", ".", vec![]);

        assert_eq!(
            interpreter
                .evaluate_expression(Expression {
                    span: Span::new((1, 1, 0), "test"),
                    value: expressions::Number { value: 0.0 }.into(),
                })
                .unwrap()
                .get_value()
                .unwrap()
                .ignore_error()
                .expect::<values::Number>()
                .unwrap()
                .value,
            0.0
        );
    }

    #[test]
    #[traced_test]
    fn not_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let mut interpreter = Interpreter::new("test", ".", vec![]);

        assert!(
            interpreter
                .evaluate_expression(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 2, 1), "test"),
                    value: expressions::Not {
                        expression: Box::new(Expression {
                            span: Span::new((1, 2, 1), "test"),
                            value: expressions::False.into(),
                        })
                    }
                    .into(),
                })
                .unwrap()
                .get_value()
                .unwrap()
                .ignore_error()
                .expect::<values::Bool>()
                .unwrap()
                .value
        );
    }

    #[test]
    #[traced_test]
    fn negate_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let mut interpreter = Interpreter::new("test", ".", vec![]);

        assert_eq!(
            interpreter
                .evaluate_expression(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 2, 1), "test"),
                    value: expressions::Negate {
                        expression: Box::new(Expression {
                            span: Span::new((1, 2, 1), "test"),
                            value: expressions::Number { value: 1.0 }.into(),
                        })
                    }
                    .into(),
                })
                .unwrap()
                .get_value()
                .unwrap()
                .ignore_error()
                .expect::<values::Number>()
                .unwrap()
                .value,
            -1.0
        );
    }

    #[test]
    #[traced_test]
    fn group_test() {
        let _guard = tracing::subscriber::set_default(get_tracer());

        let mut interpreter = Interpreter::new("test", ".", vec![]);

        assert_eq!(
            interpreter
                .evaluate_expression(Expression {
                    span: Span::new_with_end((1, 1, 0), (1, 3, 2), "test"),
                    value: expressions::Group {
                        value: Box::new(Expression {
                            span: Span::new((1, 2, 1), "test"),
                            value: expressions::Number { value: 1.0 }.into(),
                        })
                    }
                    .into(),
                })
                .unwrap()
                .get_value()
                .unwrap()
                .ignore_error()
                .expect::<values::Number>()
                .unwrap()
                .value,
            1.0
        );
    }
}
