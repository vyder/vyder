use std::process::ExitCode;

use crate::ValueResult;

#[derive(Debug)]
pub enum ControlFlow {
    Normal(ValueResult),
    Break,
    Continue,
    Return(ValueResult),
    Ev(ValueResult),
    None,
    Exit(ExitCode),
}

impl ControlFlow {
    pub fn get_value(self) -> Option<ValueResult> {
        match self {
            ControlFlow::Normal(value) => Some(value),
            ControlFlow::Return(value) => Some(value),
            _ => None,
        }
    }
}
