use proc_macro::TokenStream;
use syn::DeriveInput;

/// Implement `Into<TokenEnum>` for a struct.
#[proc_macro_derive(IntoTokenEnum)]
pub fn into_token_enum_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse::<DeriveInput>(input).unwrap();

    let name = &ast.ident;

    let gen = quote::quote! {
        impl Into<crate::TokenEnum> for #name {
            fn into(self) -> crate::TokenEnum {
                crate::TokenEnum::#name(self)
            }
        }
    };

    TokenStream::from(gen)
}

/// Implement `Into<ExpressionEnum>` for a struct.
#[proc_macro_derive(IntoExpressionEnum)]
pub fn into_expression_enum_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse::<DeriveInput>(input).unwrap();

    let name = &ast.ident;

    let gen = quote::quote! {
        impl Into<crate::ExpressionEnum> for #name {
            fn into(self) -> crate::ExpressionEnum {
                crate::ExpressionEnum::#name(self)
            }
        }
    };

    TokenStream::from(gen)
}

/// Implement `Into<StatementEnum>` for a struct.
#[proc_macro_derive(IntoStatementEnum)]
pub fn into_statement_enum_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse::<DeriveInput>(input).unwrap();

    let name = &ast.ident;

    let gen = quote::quote! {
        impl Into<crate::StatementEnum> for #name {
            fn into(self) -> crate::StatementEnum {
                crate::StatementEnum::#name(self)
            }
        }
    };

    TokenStream::from(gen)
}

/// Implement `Into<ValueEnum>` for a struct.
#[proc_macro_derive(IntoValueEnum)]
pub fn into_value_enum_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse::<DeriveInput>(input).unwrap();

    let name = &ast.ident;

    let gen = quote::quote! {
        impl Into<crate::ValueEnum> for #name {
            fn into(self) -> crate::ValueEnum {
                crate::ValueEnum::#name(self)
            }
        }
    };

    TokenStream::from(gen)
}
