use vyder::VyderRunner;

const SOURCE: &str = include_str!("./script.vyd");

fn main() {
    let runner = VyderRunner::builder()
        .library(vyder_std::get_library())
        .build();

    runner.run_script(SOURCE, "./script.vyd");
}
