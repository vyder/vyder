use std::process::ExitCode;

fn main() -> ExitCode {
    tracing_subscriber::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .init();

    vyder::VyderRunner::builder()
        .library(vyder_std::get_library())
        .build()
        .run_with_default_cli()
}
