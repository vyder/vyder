# vyder

Vyder is the scripting language I made for my 'Matura'.

## Installation

The default version containing only the standard library can be installed using
the following command:

```bash
cargo install vyder_cli
```

Windows is currently not supported.
