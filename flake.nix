{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = {
    crane,
    flake-utils,
    nixpkgs,
    self,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;
        };
        craneLib = crane.mkLib pkgs;
      in {
        devShell = pkgs.mkShell {
          packages = with pkgs; [
            cargo
            rustc
            rust-analyzer
            clippy
            rustfmt
          ];
        };
        packages.vyder_cli = craneLib.buildPackage {
          pname = "vyder";
          version = "0.3.4";
          src = ./.;
          cargoExtraArgs = "--package vyder_cli";
        };
        packages.vyder_debug_cli = craneLib.buildPackage {
          pname = "vyder_debug_cli";
          version = "0.3.4";
          src = ./.;
          cargoExtraArgs = "--package vyder_core";
        };
        packages.default = self.packages.${system}.vyder_cli;
      }
    );
}
